(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('d3-interpolate'), require('rxjs/operators'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('ngx-circular-slider', ['exports', '@angular/core', 'rxjs', 'd3-interpolate', 'rxjs/operators', '@angular/common'], factory) :
    (factory((global['ngx-circular-slider'] = {}),global.ng.core,global.rxjs,null,global.rxjs.operators,global.ng.common));
}(this, (function (exports,core,rxjs,d3Interpolate,operators,common) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ THROTTLE_DEFAULT = 50;
    var /** @type {?} */ DEFAULT_PROPS = {
        segments: 6,
        strokeWidth: 40,
        radius: 145,
        gradientColorFrom: "#ff9800",
        gradientColorTo: "#ffcf00",
        bgCircleColor: "#171717",
        showClockFace: true,
        clockFaceColor: "#9d9d9d"
    };
    var NgxCircularSliderComponent = (function () {
        function NgxCircularSliderComponent() {
            this.update = new core.EventEmitter();
            this.props = DEFAULT_PROPS;
            this.startAngle = 0;
            this.angleLength = 0;
        }
        /**
         * @param {?} evt
         * @return {?}
         */
        NgxCircularSliderComponent.extractMouseEventCoords = /**
         * @param {?} evt
         * @return {?}
         */
            function (evt) {
                var /** @type {?} */ coords = evt instanceof MouseEvent
                    ? {
                        x: evt.clientX,
                        y: evt.clientY
                    }
                    : {
                        x: evt.changedTouches.item(0).clientX,
                        y: evt.changedTouches.item(0).clientY
                    };
                return coords;
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this.setCircleCenter();
                this.onUpdate();
                this.setObservables();
            };
        /**
         * @param {?} changes
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
            function (changes) {
                if (changes.props) {
                    this.props = changes.props.firstChange
                        ? Object.assign(DEFAULT_PROPS, changes.props.currentValue)
                        : DEFAULT_PROPS;
                }
                this.onUpdate();
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
            function () {
                this.closeStreams();
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.onUpdate = /**
         * @return {?}
         */
            function () {
                this.calcStartAndStop();
                this.createSegments();
                this.update.emit({
                    startAngle: this.startAngle,
                    angleLength: this.angleLength
                });
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.setObservables = /**
         * @return {?}
         */
            function () {
                var _this = this;
                var /** @type {?} */ mouseMove$ = rxjs.merge(rxjs.fromEvent(document, "mousemove"), rxjs.fromEvent(document, "touchmove"));
                var /** @type {?} */ mouseUp$ = rxjs.merge(rxjs.fromEvent(document, "mouseup"), rxjs.fromEvent(document, "touchend"));
                //     this.startSubscription = merge(
                //       fromEvent(this.startIcon.nativeElement, "touchstart"),
                //       fromEvent(this.startIcon.nativeElement, "mousedown")
                //     )
                //       .pipe(
                //         switchMap(_ =>
                //           mouseMove$.pipe(
                //             takeUntil(mouseUp$),
                //             throttleTime(THROTTLE_DEFAULT)
                //           )
                //         )
                //       )
                //       .subscribe((res: MouseEvent | TouchEvent) => {
                //         this.handleStartPan(res);
                //       });
                this.stopSubscription = rxjs.merge(rxjs.fromEvent(this.stopIcon.nativeElement, "touchstart"), rxjs.fromEvent(this.stopIcon.nativeElement, "mousedown"))
                    .pipe(operators.switchMap(function (_) {
                    return mouseMove$.pipe(operators.takeUntil(mouseUp$), operators.throttleTime(THROTTLE_DEFAULT));
                }))
                    .subscribe(function (res) {
                    _this.handleStopPan(res);
                });
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.closeStreams = /**
         * @return {?}
         */
            function () {
                //     if (this.startSubscription) {
                //       this.startSubscription.unsubscribe();
                //       this.startSubscription = null;
                //     }
                if (this.stopSubscription) {
                    this.stopSubscription.unsubscribe();
                    this.stopSubscription = null;
                }
            };
        /**
         * @param {?} evt
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.handleStartPan = /**
         * @param {?} evt
         * @return {?}
         */
            function (evt) {
                var /** @type {?} */ coords = NgxCircularSliderComponent.extractMouseEventCoords(evt);
                this.setCircleCenter();
                var /** @type {?} */ currentAngleStop = (this.startAngle + this.angleLength) % (2 * Math.PI);
                var /** @type {?} */ newAngle = Math.atan2(coords.y - this.circleCenterY, coords.x - this.circleCenterX) +
                    Math.PI / 2;
                if (newAngle < 0) {
                    newAngle += 2 * Math.PI;
                }
                var /** @type {?} */ newAngleLength = currentAngleStop - newAngle;
                if (newAngleLength < 0) {
                    newAngleLength += 2 * Math.PI;
                }
                this.startAngle = newAngle;
                this.angleLength = newAngleLength % (2 * Math.PI);
                this.onUpdate();
            };
        /**
         * @param {?} evt
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.handleStopPan = /**
         * @param {?} evt
         * @return {?}
         */
            function (evt) {
                var /** @type {?} */ coords = NgxCircularSliderComponent.extractMouseEventCoords(evt);
                this.setCircleCenter();
                var /** @type {?} */ newAngle = Math.atan2(coords.y - this.circleCenterY, coords.x - this.circleCenterX) +
                    Math.PI / 2;
                var /** @type {?} */ newAngleLength = (newAngle - this.startAngle) % (2 * Math.PI);
                if (newAngleLength < 0) {
                    newAngleLength += 2 * Math.PI;
                }
                this.angleLength = newAngleLength;
                this.onUpdate();
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.calcStartAndStop = /**
         * @return {?}
         */
            function () {
                this.start = this.calculateArcCircle(0, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
                this.stop = this.calculateArcCircle(this.props.segments - 1, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
            };
        /**
         * @param {?} index
         * @param {?} segments
         * @param {?} gradientColorFrom
         * @param {?} gradientColorTo
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.calculateArcColor = /**
         * @param {?} index
         * @param {?} segments
         * @param {?} gradientColorFrom
         * @param {?} gradientColorTo
         * @return {?}
         */
            function (index, segments, gradientColorFrom, gradientColorTo) {
                var /** @type {?} */ interpolate = d3Interpolate.interpolateHcl(gradientColorFrom, gradientColorTo);
                return {
                    fromColor: interpolate(index / segments),
                    toColor: interpolate((index + 1) / segments)
                };
            };
        /**
         * @param {?} indexInput
         * @param {?} segments
         * @param {?} radius
         * @param {?=} startAngleInput
         * @param {?=} angleLengthInput
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.calculateArcCircle = /**
         * @param {?} indexInput
         * @param {?} segments
         * @param {?} radius
         * @param {?=} startAngleInput
         * @param {?=} angleLengthInput
         * @return {?}
         */
            function (indexInput, segments, radius, startAngleInput, angleLengthInput) {
                if (startAngleInput === void 0) {
                    startAngleInput = 0;
                }
                if (angleLengthInput === void 0) {
                    angleLengthInput = 2 * Math.PI;
                }
                // Add 0.0001 to the possible angle so when start = stop angle, whole circle is drawn
                var /** @type {?} */ startAngle = startAngleInput % (2 * Math.PI);
                var /** @type {?} */ angleLength = angleLengthInput % (2 * Math.PI);
                var /** @type {?} */ index = indexInput + 1;
                var /** @type {?} */ fromAngle = (angleLength / segments) * (index - 1) + startAngle;
                var /** @type {?} */ toAngle = (angleLength / segments) * index + startAngle;
                var /** @type {?} */ fromX = radius * Math.sin(fromAngle);
                var /** @type {?} */ fromY = -radius * Math.cos(fromAngle);
                var /** @type {?} */ realToX = radius * Math.sin(toAngle);
                var /** @type {?} */ realToY = -radius * Math.cos(toAngle);
                // add 0.005 to start drawing a little bit earlier so segments stick together
                var /** @type {?} */ toX = radius * Math.sin(toAngle + 0.005);
                var /** @type {?} */ toY = -radius * Math.cos(toAngle + 0.005);
                return {
                    fromX: fromX,
                    fromY: fromY,
                    toX: toX,
                    toY: toY,
                    realToX: realToX,
                    realToY: realToY
                };
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.createSegments = /**
         * @return {?}
         */
            function () {
                this.segments = [];
                for (var /** @type {?} */ i = 0; i < this.props.segments; i++) {
                    var /** @type {?} */ id = i;
                    var /** @type {?} */ colors = this.calculateArcColor(id, this.props.segments, this.props.gradientColorFrom, this.props.gradientColorTo);
                    var /** @type {?} */ arcs = this.calculateArcCircle(id, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
                    this.segments.push({
                        id: id,
                        d: "M " + arcs.fromX.toFixed(2) + " " + arcs.fromY.toFixed(2) + " A " + this.props.radius + " " + this.props.radius + " \n        0 0 1 " + arcs.toX.toFixed(2) + " " + arcs.toY.toFixed(2),
                        colors: Object.assign({}, colors),
                        arcs: Object.assign({}, arcs)
                    });
                }
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.setCircleCenter = /**
         * @return {?}
         */
            function () {
                // todo: nicer solution to use document.body?
                var /** @type {?} */ bodyRect = document.body.getBoundingClientRect();
                var /** @type {?} */ elemRect = this.circle.nativeElement.getBoundingClientRect();
                var /** @type {?} */ px = elemRect.left - bodyRect.left;
                var /** @type {?} */ py = elemRect.top - bodyRect.top;
                var /** @type {?} */ halfOfContainer = this.getContainerWidth() / 2;
                this.circleCenterX = px + halfOfContainer;
                this.circleCenterY = py + halfOfContainer;
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.getContainerWidth = /**
         * @return {?}
         */
            function () {
                var _a = this.props, strokeWidth = _a.strokeWidth, radius = _a.radius;
                return strokeWidth + radius * 2 + 2;
            };
        /**
         * @param {?} index
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.getGradientId = /**
         * @param {?} index
         * @return {?}
         */
            function (index) {
                return "gradient" + index;
            };
        /**
         * @param {?} index
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.getGradientUrl = /**
         * @param {?} index
         * @return {?}
         */
            function (index) {
                return 'url(' + window.location + '#gradient' + index + ')';
            };
        /**
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.getTranslate = /**
         * @return {?}
         */
            function () {
                return " translate(\n  " + (this.props.strokeWidth / 2 + this.props.radius + 1) + ",\n  " + (this.props.strokeWidth / 2 + this.props.radius + 1) + " )";
            };
        /**
         * @param {?} x
         * @param {?} y
         * @return {?}
         */
        NgxCircularSliderComponent.prototype.getTranslateFrom = /**
         * @param {?} x
         * @param {?} y
         * @return {?}
         */
            function (x, y) {
                return " translate(" + x + ", " + y + ")";
            };
        NgxCircularSliderComponent.decorators = [
            { type: core.Component, args: [{
                        selector: "ngx-cs-slider",
                        templateUrl: "./ngx-cs-slider.component.html",
                        styleUrls: ["./ngx-cs-slider.component.scss"]
                    },] },
        ];
        /** @nocollapse */
        NgxCircularSliderComponent.ctorParameters = function () { return []; };
        NgxCircularSliderComponent.propDecorators = {
            props: [{ type: core.Input }],
            startAngle: [{ type: core.Input }],
            angleLength: [{ type: core.Input }],
            update: [{ type: core.Output }],
            circle: [{ type: core.ViewChild, args: ["circle",] }],
            stopIcon: [{ type: core.ViewChild, args: ["stopIcon",] }],
            startIcon: [{ type: core.ViewChild, args: ["startIcon",] }]
        };
        return NgxCircularSliderComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ DEFAULT_RANGE = 48;
    var /** @type {?} */ DEFAULT_TIME_RANGE = 12;
    var NgXCSClockFaceComponent = (function () {
        function NgXCSClockFaceComponent() {
            this.clockLines = [];
            this.clockTexts = [];
        }
        /**
         * @return {?}
         */
        NgXCSClockFaceComponent.prototype.ngOnChanges = /**
         * @return {?}
         */
            function () {
                this.faceRadius = this.radius - 5;
                this.textRadius = this.radius - 26;
                this.createClockLines();
                this.createClockTexts();
            };
        /**
         * @return {?}
         */
        NgXCSClockFaceComponent.prototype.createClockLines = /**
         * @return {?}
         */
            function () {
                for (var /** @type {?} */ i = 0; i < DEFAULT_RANGE; i++) {
                    var /** @type {?} */ cos = Math.cos(((2 * Math.PI) / DEFAULT_RANGE) * i);
                    var /** @type {?} */ sin = Math.sin(((2 * Math.PI) / DEFAULT_RANGE) * i);
                    this.clockLines.push({
                        id: i,
                        strokeWidth: i % 4 === 0 ? 3 : 1,
                        x1: cos * this.faceRadius,
                        y1: sin * this.faceRadius,
                        x2: cos * (this.faceRadius - 7),
                        y2: sin * (this.faceRadius - 7)
                    });
                }
            };
        /**
         * @return {?}
         */
        NgXCSClockFaceComponent.prototype.createClockTexts = /**
         * @return {?}
         */
            function () {
                for (var /** @type {?} */ i = 0; i < DEFAULT_TIME_RANGE; i++) {
                    this.clockTexts.push({
                        id: i,
                        x: this.textRadius *
                            Math.cos(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6),
                        y: this.textRadius *
                            Math.sin(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6)
                    });
                }
            };
        NgXCSClockFaceComponent.decorators = [
            { type: core.Component, args: [{
                        selector: "[ngx-cs-clock-face]",
                        templateUrl: "./ngx-cs-clock-face.component.html",
                        styleUrls: ["./ngx-cs-clock-face.component.scss"]
                    },] },
        ];
        /** @nocollapse */
        NgXCSClockFaceComponent.ctorParameters = function () { return []; };
        NgXCSClockFaceComponent.propDecorators = {
            radius: [{ type: core.Input }],
            stroke: [{ type: core.Input }]
        };
        return NgXCSClockFaceComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var NgxCircularSliderModule = (function () {
        function NgxCircularSliderModule() {
        }
        NgxCircularSliderModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [common.CommonModule],
                        declarations: [NgxCircularSliderComponent, NgXCSClockFaceComponent],
                        exports: [NgxCircularSliderComponent]
                    },] },
        ];
        return NgxCircularSliderModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.NgxCircularSliderComponent = NgxCircularSliderComponent;
    exports.NgxCircularSliderModule = NgxCircularSliderModule;
    exports.ɵa = NgXCSClockFaceComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNpcmN1bGFyLXNsaWRlci51bWQuanMubWFwIiwic291cmNlcyI6WyJuZzovL25neC1jaXJjdWxhci1zbGlkZXIvbGliL25neC1jcy1zbGlkZXIvbmd4LWNzLXNsaWRlci5jb21wb25lbnQudHMiLCJuZzovL25neC1jaXJjdWxhci1zbGlkZXIvbGliL25neC1jcy1jbG9jay1mYWNlL25neC1jcy1jbG9jay1mYWNlLmNvbXBvbmVudC50cyIsIm5nOi8vbmd4LWNpcmN1bGFyLXNsaWRlci9saWIvbmd4LWNpcmN1bGFyLXNsaWRlci5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgRWxlbWVudFJlZixcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgSW5wdXQsXHJcbiAgT25DaGFuZ2VzLFxyXG4gIE9uRGVzdHJveSxcclxuICBPbkluaXQsXHJcbiAgT3V0cHV0LFxyXG4gIFZpZXdDaGlsZFxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IGZyb21FdmVudCwgbWVyZ2UsIFN1YnNjcmlwdGlvbiB9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCB7IGludGVycG9sYXRlSGNsIH0gZnJvbSBcImQzLWludGVycG9sYXRlXCI7XHJcbmltcG9ydCB7XHJcbiAgSUFyYyxcclxuICBJQ29sb3IsXHJcbiAgSUNvb3JkcyxcclxuICBJT3V0cHV0LFxyXG4gIElQcm9wcyxcclxuICBJU2VnbWVudCxcclxuICBJU2xpZGVyQ2hhbmdlc1xyXG59IGZyb20gXCIuLi9pbnRlcmZhY2VzXCI7XHJcbmltcG9ydCB7IHN3aXRjaE1hcCwgdGFrZVVudGlsLCB0aHJvdHRsZVRpbWUgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbmNvbnN0IFRIUk9UVExFX0RFRkFVTFQgPSA1MDtcclxuY29uc3QgREVGQVVMVF9QUk9QUzogSVByb3BzID0ge1xyXG4gIHNlZ21lbnRzOiA2LFxyXG4gIHN0cm9rZVdpZHRoOiA0MCxcclxuICByYWRpdXM6IDE0NSxcclxuICBncmFkaWVudENvbG9yRnJvbTogXCIjZmY5ODAwXCIsXHJcbiAgZ3JhZGllbnRDb2xvclRvOiBcIiNmZmNmMDBcIixcclxuICBiZ0NpcmNsZUNvbG9yOiBcIiMxNzE3MTdcIixcclxuICBzaG93Q2xvY2tGYWNlOiB0cnVlLFxyXG4gIGNsb2NrRmFjZUNvbG9yOiBcIiM5ZDlkOWRcIlxyXG59O1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwibmd4LWNzLXNsaWRlclwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vbmd4LWNzLXNsaWRlci5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9uZ3gtY3Mtc2xpZGVyLmNvbXBvbmVudC5zY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudFxyXG4gIGltcGxlbWVudHMgT25DaGFuZ2VzLCBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgQElucHV0KCkgcHJvcHM6IElQcm9wcztcclxuICBASW5wdXQoKSBzdGFydEFuZ2xlOiBudW1iZXI7XHJcbiAgQElucHV0KCkgYW5nbGVMZW5ndGg6IG51bWJlcjtcclxuICBAT3V0cHV0KCkgdXBkYXRlOiBFdmVudEVtaXR0ZXI8SU91dHB1dD4gPSBuZXcgRXZlbnRFbWl0dGVyPElPdXRwdXQ+KCk7XHJcbiAgcHVibGljIHNlZ21lbnRzOiBJU2VnbWVudFtdO1xyXG4gIHB1YmxpYyBzdGFydDogSUFyYztcclxuICBwdWJsaWMgc3RvcDogSUFyYztcclxuLy8gICBwcml2YXRlIHN0YXJ0U3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgcHJpdmF0ZSBzdG9wU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgcHJpdmF0ZSBjaXJjbGVDZW50ZXJYOiBudW1iZXI7XHJcbiAgcHJpdmF0ZSBjaXJjbGVDZW50ZXJZOiBudW1iZXI7XHJcbiAgQFZpZXdDaGlsZChcImNpcmNsZVwiKSBwcml2YXRlIGNpcmNsZTogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwic3RvcEljb25cIikgcHJpdmF0ZSBzdG9wSWNvbjogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKFwic3RhcnRJY29uXCIpIHByaXZhdGUgc3RhcnRJY29uOiBFbGVtZW50UmVmO1xyXG5cclxuICBwcml2YXRlIHN0YXRpYyBleHRyYWN0TW91c2VFdmVudENvb3JkcyhldnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSB7XHJcbiAgICBjb25zdCBjb29yZHM6IElDb29yZHMgPVxyXG4gICAgICBldnQgaW5zdGFuY2VvZiBNb3VzZUV2ZW50XHJcbiAgICAgICAgPyB7XHJcbiAgICAgICAgICAgIHg6IGV2dC5jbGllbnRYLFxyXG4gICAgICAgICAgICB5OiBldnQuY2xpZW50WVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIDoge1xyXG4gICAgICAgICAgICB4OiBldnQuY2hhbmdlZFRvdWNoZXMuaXRlbSgwKS5jbGllbnRYLFxyXG4gICAgICAgICAgICB5OiBldnQuY2hhbmdlZFRvdWNoZXMuaXRlbSgwKS5jbGllbnRZXHJcbiAgICAgICAgICB9O1xyXG4gICAgcmV0dXJuIGNvb3JkcztcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5wcm9wcyA9IERFRkFVTFRfUFJPUFM7XHJcbiAgICB0aGlzLnN0YXJ0QW5nbGUgPSAwO1xyXG4gICAgdGhpcy5hbmdsZUxlbmd0aCA9IDA7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuc2V0Q2lyY2xlQ2VudGVyKCk7XHJcbiAgICB0aGlzLm9uVXBkYXRlKCk7XHJcbiAgICB0aGlzLnNldE9ic2VydmFibGVzKCk7XHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBJU2xpZGVyQ2hhbmdlcykge1xyXG4gICAgaWYgKGNoYW5nZXMucHJvcHMpIHtcclxuICAgICAgdGhpcy5wcm9wcyA9IGNoYW5nZXMucHJvcHMuZmlyc3RDaGFuZ2VcclxuICAgICAgICA/IE9iamVjdC5hc3NpZ24oREVGQVVMVF9QUk9QUywgY2hhbmdlcy5wcm9wcy5jdXJyZW50VmFsdWUpXHJcbiAgICAgICAgOiBERUZBVUxUX1BST1BTO1xyXG4gICAgfVxyXG4gICAgdGhpcy5vblVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLmNsb3NlU3RyZWFtcygpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBvblVwZGF0ZSgpIHtcclxuICAgIHRoaXMuY2FsY1N0YXJ0QW5kU3RvcCgpO1xyXG4gICAgdGhpcy5jcmVhdGVTZWdtZW50cygpO1xyXG4gICAgdGhpcy51cGRhdGUuZW1pdCh7XHJcbiAgICAgIHN0YXJ0QW5nbGU6IHRoaXMuc3RhcnRBbmdsZSxcclxuICAgICAgYW5nbGVMZW5ndGg6IHRoaXMuYW5nbGVMZW5ndGhcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzZXRPYnNlcnZhYmxlcygpIHtcclxuICAgIGNvbnN0IG1vdXNlTW92ZSQgPSBtZXJnZShcclxuICAgICAgZnJvbUV2ZW50KGRvY3VtZW50LCBcIm1vdXNlbW92ZVwiKSxcclxuICAgICAgZnJvbUV2ZW50KGRvY3VtZW50LCBcInRvdWNobW92ZVwiKVxyXG4gICAgKTtcclxuICAgIGNvbnN0IG1vdXNlVXAkID0gbWVyZ2UoXHJcbiAgICAgIGZyb21FdmVudChkb2N1bWVudCwgXCJtb3VzZXVwXCIpLFxyXG4gICAgICBmcm9tRXZlbnQoZG9jdW1lbnQsIFwidG91Y2hlbmRcIilcclxuICAgICk7XHJcblxyXG4vLyAgICAgdGhpcy5zdGFydFN1YnNjcmlwdGlvbiA9IG1lcmdlKFxyXG4vLyAgICAgICBmcm9tRXZlbnQodGhpcy5zdGFydEljb24ubmF0aXZlRWxlbWVudCwgXCJ0b3VjaHN0YXJ0XCIpLFxyXG4vLyAgICAgICBmcm9tRXZlbnQodGhpcy5zdGFydEljb24ubmF0aXZlRWxlbWVudCwgXCJtb3VzZWRvd25cIilcclxuLy8gICAgIClcclxuLy8gICAgICAgLnBpcGUoXHJcbi8vICAgICAgICAgc3dpdGNoTWFwKF8gPT5cclxuLy8gICAgICAgICAgIG1vdXNlTW92ZSQucGlwZShcclxuLy8gICAgICAgICAgICAgdGFrZVVudGlsKG1vdXNlVXAkKSxcclxuLy8gICAgICAgICAgICAgdGhyb3R0bGVUaW1lKFRIUk9UVExFX0RFRkFVTFQpXHJcbi8vICAgICAgICAgICApXHJcbi8vICAgICAgICAgKVxyXG4vLyAgICAgICApXHJcbi8vICAgICAgIC5zdWJzY3JpYmUoKHJlczogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQpID0+IHtcclxuLy8gICAgICAgICB0aGlzLmhhbmRsZVN0YXJ0UGFuKHJlcyk7XHJcbi8vICAgICAgIH0pO1xyXG5cclxuICAgIHRoaXMuc3RvcFN1YnNjcmlwdGlvbiA9IG1lcmdlKFxyXG4gICAgICBmcm9tRXZlbnQodGhpcy5zdG9wSWNvbi5uYXRpdmVFbGVtZW50LCBcInRvdWNoc3RhcnRcIiksXHJcbiAgICAgIGZyb21FdmVudCh0aGlzLnN0b3BJY29uLm5hdGl2ZUVsZW1lbnQsIFwibW91c2Vkb3duXCIpXHJcbiAgICApXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIHN3aXRjaE1hcChfID0+XHJcbiAgICAgICAgICBtb3VzZU1vdmUkLnBpcGUoXHJcbiAgICAgICAgICAgIHRha2VVbnRpbChtb3VzZVVwJCksXHJcbiAgICAgICAgICAgIHRocm90dGxlVGltZShUSFJPVFRMRV9ERUZBVUxUKVxyXG4gICAgICAgICAgKVxyXG4gICAgICAgIClcclxuICAgICAgKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXM6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSA9PiB7XHJcbiAgICAgICAgdGhpcy5oYW5kbGVTdG9wUGFuKHJlcyk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjbG9zZVN0cmVhbXMoKSB7XHJcbi8vICAgICBpZiAodGhpcy5zdGFydFN1YnNjcmlwdGlvbikge1xyXG4vLyAgICAgICB0aGlzLnN0YXJ0U3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbi8vICAgICAgIHRoaXMuc3RhcnRTdWJzY3JpcHRpb24gPSBudWxsO1xyXG4vLyAgICAgfVxyXG4gICAgaWYgKHRoaXMuc3RvcFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLnN0b3BTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgICAgdGhpcy5zdG9wU3Vic2NyaXB0aW9uID0gbnVsbDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlU3RhcnRQYW4oZXZ0OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkge1xyXG4gICAgY29uc3QgY29vcmRzID0gTmd4Q2lyY3VsYXJTbGlkZXJDb21wb25lbnQuZXh0cmFjdE1vdXNlRXZlbnRDb29yZHMoZXZ0KTtcclxuXHJcbiAgICB0aGlzLnNldENpcmNsZUNlbnRlcigpO1xyXG4gICAgY29uc3QgY3VycmVudEFuZ2xlU3RvcCA9XHJcbiAgICAgICh0aGlzLnN0YXJ0QW5nbGUgKyB0aGlzLmFuZ2xlTGVuZ3RoKSAlICgyICogTWF0aC5QSSk7XHJcbiAgICBsZXQgbmV3QW5nbGUgPVxyXG4gICAgICBNYXRoLmF0YW4yKGNvb3Jkcy55IC0gdGhpcy5jaXJjbGVDZW50ZXJZLCBjb29yZHMueCAtIHRoaXMuY2lyY2xlQ2VudGVyWCkgK1xyXG4gICAgICBNYXRoLlBJIC8gMjtcclxuXHJcbiAgICBpZiAobmV3QW5nbGUgPCAwKSB7XHJcbiAgICAgIG5ld0FuZ2xlICs9IDIgKiBNYXRoLlBJO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCBuZXdBbmdsZUxlbmd0aCA9IGN1cnJlbnRBbmdsZVN0b3AgLSBuZXdBbmdsZTtcclxuICAgIGlmIChuZXdBbmdsZUxlbmd0aCA8IDApIHtcclxuICAgICAgbmV3QW5nbGVMZW5ndGggKz0gMiAqIE1hdGguUEk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zdGFydEFuZ2xlID0gbmV3QW5nbGU7XHJcbiAgICB0aGlzLmFuZ2xlTGVuZ3RoID0gbmV3QW5nbGVMZW5ndGggJSAoMiAqIE1hdGguUEkpO1xyXG5cclxuICAgIHRoaXMub25VcGRhdGUoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlU3RvcFBhbihldnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSB7XHJcbiAgICBjb25zdCBjb29yZHMgPSBOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudC5leHRyYWN0TW91c2VFdmVudENvb3JkcyhldnQpO1xyXG4gICAgdGhpcy5zZXRDaXJjbGVDZW50ZXIoKTtcclxuICAgIGNvbnN0IG5ld0FuZ2xlID1cclxuICAgICAgTWF0aC5hdGFuMihjb29yZHMueSAtIHRoaXMuY2lyY2xlQ2VudGVyWSwgY29vcmRzLnggLSB0aGlzLmNpcmNsZUNlbnRlclgpICtcclxuICAgICAgTWF0aC5QSSAvIDI7XHJcbiAgICBsZXQgbmV3QW5nbGVMZW5ndGggPSAobmV3QW5nbGUgLSB0aGlzLnN0YXJ0QW5nbGUpICUgKDIgKiBNYXRoLlBJKTtcclxuXHJcbiAgICBpZiAobmV3QW5nbGVMZW5ndGggPCAwKSB7XHJcbiAgICAgIG5ld0FuZ2xlTGVuZ3RoICs9IDIgKiBNYXRoLlBJO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuYW5nbGVMZW5ndGggPSBuZXdBbmdsZUxlbmd0aDtcclxuICAgIHRoaXMub25VcGRhdGUoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY2FsY1N0YXJ0QW5kU3RvcCgpIHtcclxuICAgIHRoaXMuc3RhcnQgPSB0aGlzLmNhbGN1bGF0ZUFyY0NpcmNsZShcclxuICAgICAgMCxcclxuICAgICAgdGhpcy5wcm9wcy5zZWdtZW50cyxcclxuICAgICAgdGhpcy5wcm9wcy5yYWRpdXMsXHJcbiAgICAgIHRoaXMuc3RhcnRBbmdsZSxcclxuICAgICAgdGhpcy5hbmdsZUxlbmd0aFxyXG4gICAgKTtcclxuICAgIHRoaXMuc3RvcCA9IHRoaXMuY2FsY3VsYXRlQXJjQ2lyY2xlKFxyXG4gICAgICB0aGlzLnByb3BzLnNlZ21lbnRzIC0gMSxcclxuICAgICAgdGhpcy5wcm9wcy5zZWdtZW50cyxcclxuICAgICAgdGhpcy5wcm9wcy5yYWRpdXMsXHJcbiAgICAgIHRoaXMuc3RhcnRBbmdsZSxcclxuICAgICAgdGhpcy5hbmdsZUxlbmd0aFxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY2FsY3VsYXRlQXJjQ29sb3IoXHJcbiAgICBpbmRleCxcclxuICAgIHNlZ21lbnRzLFxyXG4gICAgZ3JhZGllbnRDb2xvckZyb20sXHJcbiAgICBncmFkaWVudENvbG9yVG9cclxuICApIHtcclxuICAgIGNvbnN0IGludGVycG9sYXRlID0gaW50ZXJwb2xhdGVIY2woZ3JhZGllbnRDb2xvckZyb20sIGdyYWRpZW50Q29sb3JUbyk7XHJcblxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZnJvbUNvbG9yOiBpbnRlcnBvbGF0ZShpbmRleCAvIHNlZ21lbnRzKSxcclxuICAgICAgdG9Db2xvcjogaW50ZXJwb2xhdGUoKGluZGV4ICsgMSkgLyBzZWdtZW50cylcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNhbGN1bGF0ZUFyY0NpcmNsZShcclxuICAgIGluZGV4SW5wdXQsXHJcbiAgICBzZWdtZW50cyxcclxuICAgIHJhZGl1cyxcclxuICAgIHN0YXJ0QW5nbGVJbnB1dCA9IDAsXHJcbiAgICBhbmdsZUxlbmd0aElucHV0ID0gMiAqIE1hdGguUElcclxuICApIHtcclxuICAgIC8vIEFkZCAwLjAwMDEgdG8gdGhlIHBvc3NpYmxlIGFuZ2xlIHNvIHdoZW4gc3RhcnQgPSBzdG9wIGFuZ2xlLCB3aG9sZSBjaXJjbGUgaXMgZHJhd25cclxuICAgIGNvbnN0IHN0YXJ0QW5nbGUgPSBzdGFydEFuZ2xlSW5wdXQgJSAoMiAqIE1hdGguUEkpO1xyXG4gICAgY29uc3QgYW5nbGVMZW5ndGggPSBhbmdsZUxlbmd0aElucHV0ICUgKDIgKiBNYXRoLlBJKTtcclxuICAgIGNvbnN0IGluZGV4ID0gaW5kZXhJbnB1dCArIDE7XHJcbiAgICBjb25zdCBmcm9tQW5nbGUgPSAoYW5nbGVMZW5ndGggLyBzZWdtZW50cykgKiAoaW5kZXggLSAxKSArIHN0YXJ0QW5nbGU7XHJcbiAgICBjb25zdCB0b0FuZ2xlID0gKGFuZ2xlTGVuZ3RoIC8gc2VnbWVudHMpICogaW5kZXggKyBzdGFydEFuZ2xlO1xyXG4gICAgY29uc3QgZnJvbVggPSByYWRpdXMgKiBNYXRoLnNpbihmcm9tQW5nbGUpO1xyXG4gICAgY29uc3QgZnJvbVkgPSAtcmFkaXVzICogTWF0aC5jb3MoZnJvbUFuZ2xlKTtcclxuICAgIGNvbnN0IHJlYWxUb1ggPSByYWRpdXMgKiBNYXRoLnNpbih0b0FuZ2xlKTtcclxuICAgIGNvbnN0IHJlYWxUb1kgPSAtcmFkaXVzICogTWF0aC5jb3ModG9BbmdsZSk7XHJcblxyXG4gICAgLy8gYWRkIDAuMDA1IHRvIHN0YXJ0IGRyYXdpbmcgYSBsaXR0bGUgYml0IGVhcmxpZXIgc28gc2VnbWVudHMgc3RpY2sgdG9nZXRoZXJcclxuICAgIGNvbnN0IHRvWCA9IHJhZGl1cyAqIE1hdGguc2luKHRvQW5nbGUgKyAwLjAwNSk7XHJcbiAgICBjb25zdCB0b1kgPSAtcmFkaXVzICogTWF0aC5jb3ModG9BbmdsZSArIDAuMDA1KTtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBmcm9tWCxcclxuICAgICAgZnJvbVksXHJcbiAgICAgIHRvWCxcclxuICAgICAgdG9ZLFxyXG4gICAgICByZWFsVG9YLFxyXG4gICAgICByZWFsVG9ZXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjcmVhdGVTZWdtZW50cygpIHtcclxuICAgIHRoaXMuc2VnbWVudHMgPSBbXTtcclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5wcm9wcy5zZWdtZW50czsgaSsrKSB7XHJcbiAgICAgIGNvbnN0IGlkID0gaTtcclxuICAgICAgY29uc3QgY29sb3JzOiBJQ29sb3IgPSB0aGlzLmNhbGN1bGF0ZUFyY0NvbG9yKFxyXG4gICAgICAgIGlkLFxyXG4gICAgICAgIHRoaXMucHJvcHMuc2VnbWVudHMsXHJcbiAgICAgICAgdGhpcy5wcm9wcy5ncmFkaWVudENvbG9yRnJvbSxcclxuICAgICAgICB0aGlzLnByb3BzLmdyYWRpZW50Q29sb3JUb1xyXG4gICAgICApO1xyXG4gICAgICBjb25zdCBhcmNzOiBJQXJjID0gdGhpcy5jYWxjdWxhdGVBcmNDaXJjbGUoXHJcbiAgICAgICAgaWQsXHJcbiAgICAgICAgdGhpcy5wcm9wcy5zZWdtZW50cyxcclxuICAgICAgICB0aGlzLnByb3BzLnJhZGl1cyxcclxuICAgICAgICB0aGlzLnN0YXJ0QW5nbGUsXHJcbiAgICAgICAgdGhpcy5hbmdsZUxlbmd0aFxyXG4gICAgICApO1xyXG5cclxuICAgICAgdGhpcy5zZWdtZW50cy5wdXNoKHtcclxuICAgICAgICBpZDogaWQsXHJcbiAgICAgICAgZDogYE0gJHthcmNzLmZyb21YLnRvRml4ZWQoMil9ICR7YXJjcy5mcm9tWS50b0ZpeGVkKDIpfSBBICR7XHJcbiAgICAgICAgICB0aGlzLnByb3BzLnJhZGl1c1xyXG4gICAgICAgIH0gJHt0aGlzLnByb3BzLnJhZGl1c30gXHJcbiAgICAgICAgMCAwIDEgJHthcmNzLnRvWC50b0ZpeGVkKDIpfSAke2FyY3MudG9ZLnRvRml4ZWQoMil9YCxcclxuICAgICAgICBjb2xvcnM6IE9iamVjdC5hc3NpZ24oe30sIGNvbG9ycyksXHJcbiAgICAgICAgYXJjczogT2JqZWN0LmFzc2lnbih7fSwgYXJjcylcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNldENpcmNsZUNlbnRlcigpIHtcclxuICAgIC8vIHRvZG86IG5pY2VyIHNvbHV0aW9uIHRvIHVzZSBkb2N1bWVudC5ib2R5P1xyXG4gICAgY29uc3QgYm9keVJlY3QgPSBkb2N1bWVudC5ib2R5LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgY29uc3QgZWxlbVJlY3QgPSB0aGlzLmNpcmNsZS5uYXRpdmVFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG4gICAgY29uc3QgcHggPSBlbGVtUmVjdC5sZWZ0IC0gYm9keVJlY3QubGVmdDtcclxuICAgIGNvbnN0IHB5ID0gZWxlbVJlY3QudG9wIC0gYm9keVJlY3QudG9wO1xyXG4gICAgY29uc3QgaGFsZk9mQ29udGFpbmVyID0gdGhpcy5nZXRDb250YWluZXJXaWR0aCgpIC8gMjtcclxuICAgIHRoaXMuY2lyY2xlQ2VudGVyWCA9IHB4ICsgaGFsZk9mQ29udGFpbmVyO1xyXG4gICAgdGhpcy5jaXJjbGVDZW50ZXJZID0gcHkgKyBoYWxmT2ZDb250YWluZXI7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0Q29udGFpbmVyV2lkdGgoKSB7XHJcbiAgICBjb25zdCB7IHN0cm9rZVdpZHRoLCByYWRpdXMgfSA9IHRoaXMucHJvcHM7XHJcbiAgICByZXR1cm4gc3Ryb2tlV2lkdGggKyByYWRpdXMgKiAyICsgMjtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRHcmFkaWVudElkKGluZGV4KSB7XHJcbiAgICByZXR1cm4gYGdyYWRpZW50JHtpbmRleH1gO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldEdyYWRpZW50VXJsKGluZGV4KSB7XHJcbiAgICByZXR1cm4gJ3VybCgnK3dpbmRvdy5sb2NhdGlvbisnI2dyYWRpZW50JytpbmRleCsnKSc7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VHJhbnNsYXRlKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gYCB0cmFuc2xhdGUoXHJcbiAgJHt0aGlzLnByb3BzLnN0cm9rZVdpZHRoIC8gMiArIHRoaXMucHJvcHMucmFkaXVzICsgMX0sXHJcbiAgJHt0aGlzLnByb3BzLnN0cm9rZVdpZHRoIC8gMiArIHRoaXMucHJvcHMucmFkaXVzICsgMX0gKWA7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VHJhbnNsYXRlRnJvbSh4LCB5KTogc3RyaW5nIHtcclxuICAgIHJldHVybiBgIHRyYW5zbGF0ZSgke3h9LCAke3l9KWA7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcyB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmNvbnN0IERFRkFVTFRfUkFOR0UgPSA0ODtcbmNvbnN0IERFRkFVTFRfVElNRV9SQU5HRSA9IDEyO1xuXG5leHBvcnQgaW50ZXJmYWNlIElDbG9ja0xpbmVzIHtcbiAgaWQ6IG51bWJlcjtcbiAgc3Ryb2tlV2lkdGg6IG51bWJlcjtcbiAgeDE6IG51bWJlcjtcbiAgeTE6IG51bWJlcjtcbiAgeDI6IG51bWJlcjtcbiAgeTI6IG51bWJlcjtcbn1cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIltuZ3gtY3MtY2xvY2stZmFjZV1cIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9uZ3gtY3MtY2xvY2stZmFjZS5jb21wb25lbnQuaHRtbFwiLFxuICBzdHlsZVVybHM6IFtcIi4vbmd4LWNzLWNsb2NrLWZhY2UuY29tcG9uZW50LnNjc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgTmdYQ1NDbG9ja0ZhY2VDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xuICBASW5wdXQoKSByYWRpdXM6IG51bWJlcjtcbiAgQElucHV0KCkgc3Ryb2tlOiBudW1iZXI7XG5cbiAgcHVibGljIGZhY2VSYWRpdXM6IG51bWJlcjtcbiAgcHVibGljIHRleHRSYWRpdXM6IG51bWJlcjtcbiAgcHVibGljIGNsb2NrTGluZXM6IElDbG9ja0xpbmVzW107XG4gIHB1YmxpYyBjbG9ja1RleHRzOiBhbnlbXTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLmNsb2NrTGluZXMgPSBbXTtcbiAgICB0aGlzLmNsb2NrVGV4dHMgPSBbXTtcbiAgfVxuXG4gIG5nT25DaGFuZ2VzKCkge1xuICAgIHRoaXMuZmFjZVJhZGl1cyA9IHRoaXMucmFkaXVzIC0gNTtcbiAgICB0aGlzLnRleHRSYWRpdXMgPSB0aGlzLnJhZGl1cyAtIDI2O1xuXG4gICAgdGhpcy5jcmVhdGVDbG9ja0xpbmVzKCk7XG4gICAgdGhpcy5jcmVhdGVDbG9ja1RleHRzKCk7XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZUNsb2NrTGluZXMoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBERUZBVUxUX1JBTkdFOyBpKyspIHtcbiAgICAgIGNvbnN0IGNvcyA9IE1hdGguY29zKCgoMiAqIE1hdGguUEkpIC8gREVGQVVMVF9SQU5HRSkgKiBpKTtcbiAgICAgIGNvbnN0IHNpbiA9IE1hdGguc2luKCgoMiAqIE1hdGguUEkpIC8gREVGQVVMVF9SQU5HRSkgKiBpKTtcbiAgICAgIHRoaXMuY2xvY2tMaW5lcy5wdXNoKHtcbiAgICAgICAgaWQ6IGksXG4gICAgICAgIHN0cm9rZVdpZHRoOiBpICUgNCA9PT0gMCA/IDMgOiAxLFxuICAgICAgICB4MTogY29zICogdGhpcy5mYWNlUmFkaXVzLFxuICAgICAgICB5MTogc2luICogdGhpcy5mYWNlUmFkaXVzLFxuICAgICAgICB4MjogY29zICogKHRoaXMuZmFjZVJhZGl1cyAtIDcpLFxuICAgICAgICB5Mjogc2luICogKHRoaXMuZmFjZVJhZGl1cyAtIDcpXG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZUNsb2NrVGV4dHMoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBERUZBVUxUX1RJTUVfUkFOR0U7IGkrKykge1xuICAgICAgdGhpcy5jbG9ja1RleHRzLnB1c2goe1xuICAgICAgICBpZDogaSxcbiAgICAgICAgeDpcbiAgICAgICAgICB0aGlzLnRleHRSYWRpdXMgKlxuICAgICAgICAgIE1hdGguY29zKCgoMiAqIE1hdGguUEkpIC8gMTIpICogaSAtIE1hdGguUEkgLyAyICsgTWF0aC5QSSAvIDYpLFxuICAgICAgICB5OlxuICAgICAgICAgIHRoaXMudGV4dFJhZGl1cyAqXG4gICAgICAgICAgTWF0aC5zaW4oKCgyICogTWF0aC5QSSkgLyAxMikgKiBpIC0gTWF0aC5QSSAvIDIgKyBNYXRoLlBJIC8gNilcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xuaW1wb3J0IHsgTmd4Q2lyY3VsYXJTbGlkZXJDb21wb25lbnQgfSBmcm9tIFwiLi9uZ3gtY3Mtc2xpZGVyL25neC1jcy1zbGlkZXIuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBOZ1hDU0Nsb2NrRmFjZUNvbXBvbmVudCB9IGZyb20gXCIuL25neC1jcy1jbG9jay1mYWNlL25neC1jcy1jbG9jay1mYWNlLmNvbXBvbmVudFwiO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcbiAgZGVjbGFyYXRpb25zOiBbTmd4Q2lyY3VsYXJTbGlkZXJDb21wb25lbnQsIE5nWENTQ2xvY2tGYWNlQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW05neENpcmN1bGFyU2xpZGVyQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBOZ3hDaXJjdWxhclNsaWRlck1vZHVsZSB7fVxuIl0sIm5hbWVzIjpbIkV2ZW50RW1pdHRlciIsIm1lcmdlIiwiZnJvbUV2ZW50Iiwic3dpdGNoTWFwIiwidGFrZVVudGlsIiwidGhyb3R0bGVUaW1lIiwiaW50ZXJwb2xhdGVIY2wiLCJDb21wb25lbnQiLCJJbnB1dCIsIk91dHB1dCIsIlZpZXdDaGlsZCIsIk5nTW9kdWxlIiwiQ29tbW9uTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsSUF3QkEscUJBQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO0lBQzVCLHFCQUFNLGFBQWEsR0FBVztRQUM1QixRQUFRLEVBQUUsQ0FBQztRQUNYLFdBQVcsRUFBRSxFQUFFO1FBQ2YsTUFBTSxFQUFFLEdBQUc7UUFDWCxpQkFBaUIsRUFBRSxTQUFTO1FBQzVCLGVBQWUsRUFBRSxTQUFTO1FBQzFCLGFBQWEsRUFBRSxTQUFTO1FBQ3hCLGFBQWEsRUFBRSxJQUFJO1FBQ25CLGNBQWMsRUFBRSxTQUFTO0tBQzFCLENBQUM7O1FBc0NBOzBCQTFCMEMsSUFBSUEsaUJBQVksRUFBVztZQTJCbkUsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7WUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7WUFDcEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7U0FDdEI7Ozs7O1FBbEJjLGtEQUF1Qjs7OztzQkFBQyxHQUE0QjtnQkFDakUscUJBQU0sTUFBTSxHQUNWLEdBQUcsWUFBWSxVQUFVO3NCQUNyQjt3QkFDRSxDQUFDLEVBQUUsR0FBRyxDQUFDLE9BQU87d0JBQ2QsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxPQUFPO3FCQUNmO3NCQUNEO3dCQUNFLENBQUMsRUFBRSxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO3dCQUNyQyxDQUFDLEVBQUUsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztxQkFDdEMsQ0FBQztnQkFDUixPQUFPLE1BQU0sQ0FBQzs7Ozs7UUFTaEIsNkNBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNoQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7YUFDdkI7Ozs7O1FBRUQsZ0RBQVc7Ozs7WUFBWCxVQUFZLE9BQXVCO2dCQUNqQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUU7b0JBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxXQUFXOzBCQUNsQyxNQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQzswQkFDeEQsYUFBYSxDQUFDO2lCQUNuQjtnQkFDRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7Ozs7UUFFRCxnREFBVzs7O1lBQVg7Z0JBQ0UsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2FBQ3JCOzs7O1FBRU8sNkNBQVE7Ozs7Z0JBQ2QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7b0JBQ2YsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO29CQUMzQixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7aUJBQzlCLENBQUMsQ0FBQzs7Ozs7UUFHRyxtREFBYzs7Ozs7Z0JBQ3BCLHFCQUFNLFVBQVUsR0FBR0MsVUFBSyxDQUN0QkMsY0FBUyxDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsRUFDaENBLGNBQVMsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQ2pDLENBQUM7Z0JBQ0YscUJBQU0sUUFBUSxHQUFHRCxVQUFLLENBQ3BCQyxjQUFTLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxFQUM5QkEsY0FBUyxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FDaEMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztnQkFrQkYsSUFBSSxDQUFDLGdCQUFnQixHQUFHRCxVQUFLLENBQzNCQyxjQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLEVBQ3BEQSxjQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQ3BEO3FCQUNFLElBQUksQ0FDSEMsbUJBQVMsQ0FBQyxVQUFBLENBQUM7b0JBQ1QsT0FBQSxVQUFVLENBQUMsSUFBSSxDQUNiQyxtQkFBUyxDQUFDLFFBQVEsQ0FBQyxFQUNuQkMsc0JBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUMvQjtpQkFBQSxDQUNGLENBQ0Y7cUJBQ0EsU0FBUyxDQUFDLFVBQUMsR0FBNEI7b0JBQ3RDLEtBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ3pCLENBQUMsQ0FBQzs7Ozs7UUFHQyxpREFBWTs7Ozs7Ozs7Z0JBS2xCLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO29CQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7aUJBQzlCOzs7Ozs7UUFHSyxtREFBYzs7OztzQkFBQyxHQUE0QjtnQkFDakQscUJBQU0sTUFBTSxHQUFHLDBCQUEwQixDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUV2RSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQ3ZCLHFCQUFNLGdCQUFnQixHQUNwQixDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN2RCxxQkFBSSxRQUFRLEdBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO29CQUN4RSxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFFZCxJQUFJLFFBQVEsR0FBRyxDQUFDLEVBQUU7b0JBQ2hCLFFBQVEsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztpQkFDekI7Z0JBRUQscUJBQUksY0FBYyxHQUFHLGdCQUFnQixHQUFHLFFBQVEsQ0FBQztnQkFDakQsSUFBSSxjQUFjLEdBQUcsQ0FBQyxFQUFFO29CQUN0QixjQUFjLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7aUJBQy9CO2dCQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO2dCQUMzQixJQUFJLENBQUMsV0FBVyxHQUFHLGNBQWMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUVsRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Ozs7OztRQUdWLGtEQUFhOzs7O3NCQUFDLEdBQTRCO2dCQUNoRCxxQkFBTSxNQUFNLEdBQUcsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3ZFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIscUJBQU0sUUFBUSxHQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztvQkFDeEUsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2QscUJBQUksY0FBYyxHQUFHLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFFbEUsSUFBSSxjQUFjLEdBQUcsQ0FBQyxFQUFFO29CQUN0QixjQUFjLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7aUJBQy9CO2dCQUVELElBQUksQ0FBQyxXQUFXLEdBQUcsY0FBYyxDQUFDO2dCQUNsQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Ozs7O1FBR1YscURBQWdCOzs7O2dCQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FDbEMsQ0FBQyxFQUNELElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFDakIsSUFBSSxDQUFDLFVBQVUsRUFDZixJQUFJLENBQUMsV0FBVyxDQUNqQixDQUFDO2dCQUNGLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxDQUFDLEVBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFDakIsSUFBSSxDQUFDLFVBQVUsRUFDZixJQUFJLENBQUMsV0FBVyxDQUNqQixDQUFDOzs7Ozs7Ozs7UUFHSSxzREFBaUI7Ozs7Ozs7c0JBQ3ZCLEtBQUssRUFDTCxRQUFRLEVBQ1IsaUJBQWlCLEVBQ2pCLGVBQWU7Z0JBRWYscUJBQU0sV0FBVyxHQUFHQyw0QkFBYyxDQUFDLGlCQUFpQixFQUFFLGVBQWUsQ0FBQyxDQUFDO2dCQUV2RSxPQUFPO29CQUNMLFNBQVMsRUFBRSxXQUFXLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztvQkFDeEMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksUUFBUSxDQUFDO2lCQUM3QyxDQUFDOzs7Ozs7Ozs7O1FBR0ksdURBQWtCOzs7Ozs7OztzQkFDeEIsVUFBVSxFQUNWLFFBQVEsRUFDUixNQUFNLEVBQ04sZUFBbUIsRUFDbkIsZ0JBQThCO2dCQUQ5QixnQ0FBQTtvQkFBQSxtQkFBbUI7O2dCQUNuQixpQ0FBQTtvQkFBQSxtQkFBbUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFOzs7Z0JBRzlCLHFCQUFNLFVBQVUsR0FBRyxlQUFlLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbkQscUJBQU0sV0FBVyxHQUFHLGdCQUFnQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3JELHFCQUFNLEtBQUssR0FBRyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2dCQUM3QixxQkFBTSxTQUFTLEdBQUcsQ0FBQyxXQUFXLEdBQUcsUUFBUSxLQUFLLEtBQUssR0FBRyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUM7Z0JBQ3RFLHFCQUFNLE9BQU8sR0FBRyxDQUFDLFdBQVcsR0FBRyxRQUFRLElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQztnQkFDOUQscUJBQU0sS0FBSyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUMzQyxxQkFBTSxLQUFLLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDNUMscUJBQU0sT0FBTyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMzQyxxQkFBTSxPQUFPLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7Z0JBRzVDLHFCQUFNLEdBQUcsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUM7Z0JBQy9DLHFCQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsQ0FBQztnQkFFaEQsT0FBTztvQkFDTCxLQUFLLE9BQUE7b0JBQ0wsS0FBSyxPQUFBO29CQUNMLEdBQUcsS0FBQTtvQkFDSCxHQUFHLEtBQUE7b0JBQ0gsT0FBTyxTQUFBO29CQUNQLE9BQU8sU0FBQTtpQkFDUixDQUFDOzs7OztRQUdJLG1EQUFjOzs7O2dCQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztnQkFDbkIsS0FBSyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDNUMscUJBQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDYixxQkFBTSxNQUFNLEdBQVcsSUFBSSxDQUFDLGlCQUFpQixDQUMzQyxFQUFFLEVBQ0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUMzQixDQUFDO29CQUNGLHFCQUFNLElBQUksR0FBUyxJQUFJLENBQUMsa0JBQWtCLENBQ3hDLEVBQUUsRUFDRixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQ2pCLElBQUksQ0FBQyxVQUFVLEVBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztvQkFFRixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQzt3QkFDakIsRUFBRSxFQUFFLEVBQUU7d0JBQ04sQ0FBQyxFQUFFLE9BQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFdBQ3BELElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxTQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSx5QkFDYixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsU0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUc7d0JBQ3BELE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUM7d0JBQ2pDLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUM7cUJBQzlCLENBQUMsQ0FBQztpQkFDSjs7Ozs7UUFHSyxvREFBZTs7Ozs7Z0JBRXJCLHFCQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7Z0JBQ3ZELHFCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2dCQUNuRSxxQkFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUN6QyxxQkFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDO2dCQUN2QyxxQkFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsR0FBRyxlQUFlLENBQUM7Z0JBQzFDLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxHQUFHLGVBQWUsQ0FBQzs7Ozs7UUFHckMsc0RBQWlCOzs7O2dCQUN0QixxQkFBUSw0QkFBVyxFQUFFLGtCQUFNLENBQWdCO2dCQUMzQyxPQUFPLFdBQVcsR0FBRyxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQzs7Ozs7O1FBRy9CLGtEQUFhOzs7O3NCQUFDLEtBQUs7Z0JBQ3hCLE9BQU8sYUFBVyxLQUFPLENBQUM7Ozs7OztRQUdyQixtREFBYzs7OztzQkFBQyxLQUFLO2dCQUN6QixPQUFPLE1BQU0sR0FBQyxNQUFNLENBQUMsUUFBUSxHQUFDLFdBQVcsR0FBQyxLQUFLLEdBQUMsR0FBRyxDQUFDOzs7OztRQUcvQyxpREFBWTs7OztnQkFDakIsT0FBTyxxQkFDUCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxlQUNsRCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxRQUFJLENBQUM7Ozs7Ozs7UUFHbEQscURBQWdCOzs7OztzQkFBQyxDQUFDLEVBQUUsQ0FBQztnQkFDMUIsT0FBTyxnQkFBYyxDQUFDLFVBQUssQ0FBQyxNQUFHLENBQUM7OztvQkFqU25DQyxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGVBQWU7d0JBQ3pCLFdBQVcsRUFBRSxnQ0FBZ0M7d0JBQzdDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO3FCQUM5Qzs7Ozs7NEJBR0VDLFVBQUs7aUNBQ0xBLFVBQUs7a0NBQ0xBLFVBQUs7NkJBQ0xDLFdBQU07NkJBUU5DLGNBQVMsU0FBQyxRQUFROytCQUNsQkEsY0FBUyxTQUFDLFVBQVU7Z0NBQ3BCQSxjQUFTLFNBQUMsV0FBVzs7eUNBeER4Qjs7Ozs7OztBQ0FBLElBRUEscUJBQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztJQUN6QixxQkFBTSxrQkFBa0IsR0FBRyxFQUFFLENBQUM7O1FBeUI1QjtZQUNFLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1NBQ3RCOzs7O1FBRUQsNkNBQVc7OztZQUFYO2dCQUNFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7Z0JBRW5DLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUN4QixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUN6Qjs7OztRQUVPLGtEQUFnQjs7OztnQkFDdEIsS0FBSyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3RDLHFCQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsSUFBSSxhQUFhLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzFELHFCQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsSUFBSSxhQUFhLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzFELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO3dCQUNuQixFQUFFLEVBQUUsQ0FBQzt3QkFDTCxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7d0JBQ2hDLEVBQUUsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVU7d0JBQ3pCLEVBQUUsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVU7d0JBQ3pCLEVBQUUsRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7d0JBQy9CLEVBQUUsRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7cUJBQ2hDLENBQUMsQ0FBQztpQkFDSjs7Ozs7UUFHSyxrREFBZ0I7Ozs7Z0JBQ3RCLEtBQUsscUJBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsa0JBQWtCLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQzNDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO3dCQUNuQixFQUFFLEVBQUUsQ0FBQzt3QkFDTCxDQUFDLEVBQ0MsSUFBSSxDQUFDLFVBQVU7NEJBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzt3QkFDaEUsQ0FBQyxFQUNDLElBQUksQ0FBQyxVQUFVOzRCQUNmLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7cUJBQ2pFLENBQUMsQ0FBQztpQkFDSjs7O29CQXJESkgsY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxxQkFBcUI7d0JBQy9CLFdBQVcsRUFBRSxvQ0FBb0M7d0JBQ2pELFNBQVMsRUFBRSxDQUFDLG9DQUFvQyxDQUFDO3FCQUNsRDs7Ozs7NkJBRUVDLFVBQUs7NkJBQ0xBLFVBQUs7O3NDQXJCUjs7Ozs7OztBQ0FBOzs7O29CQUtDRyxhQUFRLFNBQUM7d0JBQ1IsT0FBTyxFQUFFLENBQUNDLG1CQUFZLENBQUM7d0JBQ3ZCLFlBQVksRUFBRSxDQUFDLDBCQUEwQixFQUFFLHVCQUF1QixDQUFDO3dCQUNuRSxPQUFPLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztxQkFDdEM7O3NDQVREOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9