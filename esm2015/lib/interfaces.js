/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */
export function ISliderChanges() { }
function ISliderChanges_tsickle_Closure_declarations() {
    /** @type {?} */
    ISliderChanges.prototype.props;
}
/**
 * @record
 */
export function IOutput() { }
function IOutput_tsickle_Closure_declarations() {
    /** @type {?} */
    IOutput.prototype.angleLength;
    /** @type {?} */
    IOutput.prototype.startAngle;
}
/**
 * @record
 */
export function IProps() { }
function IProps_tsickle_Closure_declarations() {
    /** @type {?|undefined} */
    IProps.prototype.segments;
    /** @type {?|undefined} */
    IProps.prototype.strokeWidth;
    /** @type {?|undefined} */
    IProps.prototype.radius;
    /** @type {?|undefined} */
    IProps.prototype.gradientColorFrom;
    /** @type {?|undefined} */
    IProps.prototype.gradientColorTo;
    /** @type {?|undefined} */
    IProps.prototype.bgCircleColor;
    /** @type {?|undefined} */
    IProps.prototype.showClockFace;
    /** @type {?|undefined} */
    IProps.prototype.clockFaceColor;
}
/**
 * @record
 */
export function IArc() { }
function IArc_tsickle_Closure_declarations() {
    /** @type {?} */
    IArc.prototype.fromX;
    /** @type {?} */
    IArc.prototype.fromY;
    /** @type {?} */
    IArc.prototype.toX;
    /** @type {?} */
    IArc.prototype.toY;
    /** @type {?} */
    IArc.prototype.realToX;
    /** @type {?} */
    IArc.prototype.realToY;
}
/**
 * @record
 */
export function IColor() { }
function IColor_tsickle_Closure_declarations() {
    /** @type {?} */
    IColor.prototype.fromColor;
    /** @type {?} */
    IColor.prototype.toColor;
}
/**
 * @record
 */
export function ICoords() { }
function ICoords_tsickle_Closure_declarations() {
    /** @type {?} */
    ICoords.prototype.x;
    /** @type {?} */
    ICoords.prototype.y;
}
/**
 * @record
 */
export function ISegment() { }
function ISegment_tsickle_Closure_declarations() {
    /** @type {?} */
    ISegment.prototype.id;
    /** @type {?} */
    ISegment.prototype.d;
    /** @type {?} */
    ISegment.prototype.colors;
    /** @type {?} */
    ISegment.prototype.arcs;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jaXJjdWxhci1zbGlkZXIvIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU2ltcGxlQ2hhbmdlLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuZXhwb3J0IGludGVyZmFjZSBJU2xpZGVyQ2hhbmdlcyBleHRlbmRzIFNpbXBsZUNoYW5nZXMge1xuICBwcm9wczogU2ltcGxlQ2hhbmdlO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElPdXRwdXQge1xuICBhbmdsZUxlbmd0aDogbnVtYmVyO1xuICBzdGFydEFuZ2xlOiBudW1iZXI7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVByb3BzIHtcbiAgc2VnbWVudHM/OiBudW1iZXI7XG4gIHN0cm9rZVdpZHRoPzogbnVtYmVyO1xuICByYWRpdXM/OiBudW1iZXI7XG4gIGdyYWRpZW50Q29sb3JGcm9tPzogc3RyaW5nO1xuICBncmFkaWVudENvbG9yVG8/OiBzdHJpbmc7XG4gIGJnQ2lyY2xlQ29sb3I/OiBzdHJpbmc7XG4gIHNob3dDbG9ja0ZhY2U/OiBib29sZWFuO1xuICBjbG9ja0ZhY2VDb2xvcj86IHN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQXJjIHtcbiAgZnJvbVg6IG51bWJlcjtcbiAgZnJvbVk6IG51bWJlcjtcbiAgdG9YOiBudW1iZXI7XG4gIHRvWTogbnVtYmVyO1xuICByZWFsVG9YOiBudW1iZXI7XG4gIHJlYWxUb1k6IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQ29sb3Ige1xuICBmcm9tQ29sb3I6IHN0cmluZztcbiAgdG9Db2xvcjogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElDb29yZHMge1xuICB4OiBudW1iZXI7XG4gIHk6IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJU2VnbWVudCB7XG4gIGlkOiBudW1iZXI7XG4gIGQ6IHN0cmluZztcbiAgY29sb3JzOiBJQ29sb3I7XG4gIGFyY3M6IElBcmM7XG59XG4iXX0=