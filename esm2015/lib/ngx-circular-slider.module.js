/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgxCircularSliderComponent } from "./ngx-cs-slider/ngx-cs-slider.component";
import { NgXCSClockFaceComponent } from "./ngx-cs-clock-face/ngx-cs-clock-face.component";
export class NgxCircularSliderModule {
}
NgxCircularSliderModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                declarations: [NgxCircularSliderComponent, NgXCSClockFaceComponent],
                exports: [NgxCircularSliderComponent]
            },] },
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNpcmN1bGFyLXNsaWRlci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY2lyY3VsYXItc2xpZGVyLyIsInNvdXJjZXMiOlsibGliL25neC1jaXJjdWxhci1zbGlkZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQU8xRixNQUFNOzs7WUFMTCxRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2dCQUN2QixZQUFZLEVBQUUsQ0FBQywwQkFBMEIsRUFBRSx1QkFBdUIsQ0FBQztnQkFDbkUsT0FBTyxFQUFFLENBQUMsMEJBQTBCLENBQUM7YUFDdEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XG5pbXBvcnQgeyBOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudCB9IGZyb20gXCIuL25neC1jcy1zbGlkZXIvbmd4LWNzLXNsaWRlci5jb21wb25lbnRcIjtcbmltcG9ydCB7IE5nWENTQ2xvY2tGYWNlQ29tcG9uZW50IH0gZnJvbSBcIi4vbmd4LWNzLWNsb2NrLWZhY2Uvbmd4LWNzLWNsb2NrLWZhY2UuY29tcG9uZW50XCI7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxuICBkZWNsYXJhdGlvbnM6IFtOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudCwgTmdYQ1NDbG9ja0ZhY2VDb21wb25lbnRdLFxuICBleHBvcnRzOiBbTmd4Q2lyY3VsYXJTbGlkZXJDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIE5neENpcmN1bGFyU2xpZGVyTW9kdWxlIHt9XG4iXX0=