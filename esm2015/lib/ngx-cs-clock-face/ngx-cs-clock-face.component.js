/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from "@angular/core";
const /** @type {?} */ DEFAULT_RANGE = 48;
const /** @type {?} */ DEFAULT_TIME_RANGE = 12;
/**
 * @record
 */
export function IClockLines() { }
function IClockLines_tsickle_Closure_declarations() {
    /** @type {?} */
    IClockLines.prototype.id;
    /** @type {?} */
    IClockLines.prototype.strokeWidth;
    /** @type {?} */
    IClockLines.prototype.x1;
    /** @type {?} */
    IClockLines.prototype.y1;
    /** @type {?} */
    IClockLines.prototype.x2;
    /** @type {?} */
    IClockLines.prototype.y2;
}
export class NgXCSClockFaceComponent {
    constructor() {
        this.clockLines = [];
        this.clockTexts = [];
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.faceRadius = this.radius - 5;
        this.textRadius = this.radius - 26;
        this.createClockLines();
        this.createClockTexts();
    }
    /**
     * @return {?}
     */
    createClockLines() {
        for (let /** @type {?} */ i = 0; i < DEFAULT_RANGE; i++) {
            const /** @type {?} */ cos = Math.cos(((2 * Math.PI) / DEFAULT_RANGE) * i);
            const /** @type {?} */ sin = Math.sin(((2 * Math.PI) / DEFAULT_RANGE) * i);
            this.clockLines.push({
                id: i,
                strokeWidth: i % 4 === 0 ? 3 : 1,
                x1: cos * this.faceRadius,
                y1: sin * this.faceRadius,
                x2: cos * (this.faceRadius - 7),
                y2: sin * (this.faceRadius - 7)
            });
        }
    }
    /**
     * @return {?}
     */
    createClockTexts() {
        for (let /** @type {?} */ i = 0; i < DEFAULT_TIME_RANGE; i++) {
            this.clockTexts.push({
                id: i,
                x: this.textRadius *
                    Math.cos(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6),
                y: this.textRadius *
                    Math.sin(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6)
            });
        }
    }
}
NgXCSClockFaceComponent.decorators = [
    { type: Component, args: [{
                selector: "[ngx-cs-clock-face]",
                templateUrl: "./ngx-cs-clock-face.component.html",
                styleUrls: ["./ngx-cs-clock-face.component.scss"]
            },] },
];
/** @nocollapse */
NgXCSClockFaceComponent.ctorParameters = () => [];
NgXCSClockFaceComponent.propDecorators = {
    radius: [{ type: Input }],
    stroke: [{ type: Input }]
};
function NgXCSClockFaceComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.radius;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.stroke;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.faceRadius;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.textRadius;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.clockLines;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.clockTexts;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNzLWNsb2NrLWZhY2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWNpcmN1bGFyLXNsaWRlci8iLCJzb3VyY2VzIjpbImxpYi9uZ3gtY3MtY2xvY2stZmFjZS9uZ3gtY3MtY2xvY2stZmFjZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBRTVELHVCQUFNLGFBQWEsR0FBRyxFQUFFLENBQUM7QUFDekIsdUJBQU0sa0JBQWtCLEdBQUcsRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0I5QixNQUFNO0lBU0o7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztLQUN0Qjs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFFbkMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7S0FDekI7Ozs7SUFFTyxnQkFBZ0I7UUFDdEIsR0FBRyxDQUFDLENBQUMscUJBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDdkMsdUJBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDMUQsdUJBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDMUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLEVBQUUsRUFBRSxDQUFDO2dCQUNMLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxFQUFFLEVBQUUsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVO2dCQUN6QixFQUFFLEVBQUUsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVO2dCQUN6QixFQUFFLEVBQUUsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7Z0JBQy9CLEVBQUUsRUFBRSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQzthQUNoQyxDQUFDLENBQUM7U0FDSjs7Ozs7SUFHSyxnQkFBZ0I7UUFDdEIsR0FBRyxDQUFDLENBQUMscUJBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsa0JBQWtCLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDbkIsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsQ0FBQyxFQUNDLElBQUksQ0FBQyxVQUFVO29CQUNmLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoRSxDQUFDLEVBQ0MsSUFBSSxDQUFDLFVBQVU7b0JBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDakUsQ0FBQyxDQUFDO1NBQ0o7Ozs7WUFyREosU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLFdBQVcsRUFBRSxvQ0FBb0M7Z0JBQ2pELFNBQVMsRUFBRSxDQUFDLG9DQUFvQyxDQUFDO2FBQ2xEOzs7OztxQkFFRSxLQUFLO3FCQUNMLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5jb25zdCBERUZBVUxUX1JBTkdFID0gNDg7XG5jb25zdCBERUZBVUxUX1RJTUVfUkFOR0UgPSAxMjtcblxuZXhwb3J0IGludGVyZmFjZSBJQ2xvY2tMaW5lcyB7XG4gIGlkOiBudW1iZXI7XG4gIHN0cm9rZVdpZHRoOiBudW1iZXI7XG4gIHgxOiBudW1iZXI7XG4gIHkxOiBudW1iZXI7XG4gIHgyOiBudW1iZXI7XG4gIHkyOiBudW1iZXI7XG59XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJbbmd4LWNzLWNsb2NrLWZhY2VdXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vbmd4LWNzLWNsb2NrLWZhY2UuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL25neC1jcy1jbG9jay1mYWNlLmNvbXBvbmVudC5zY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIE5nWENTQ2xvY2tGYWNlQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcbiAgQElucHV0KCkgcmFkaXVzOiBudW1iZXI7XG4gIEBJbnB1dCgpIHN0cm9rZTogbnVtYmVyO1xuXG4gIHB1YmxpYyBmYWNlUmFkaXVzOiBudW1iZXI7XG4gIHB1YmxpYyB0ZXh0UmFkaXVzOiBudW1iZXI7XG4gIHB1YmxpYyBjbG9ja0xpbmVzOiBJQ2xvY2tMaW5lc1tdO1xuICBwdWJsaWMgY2xvY2tUZXh0czogYW55W107XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5jbG9ja0xpbmVzID0gW107XG4gICAgdGhpcy5jbG9ja1RleHRzID0gW107XG4gIH1cblxuICBuZ09uQ2hhbmdlcygpIHtcbiAgICB0aGlzLmZhY2VSYWRpdXMgPSB0aGlzLnJhZGl1cyAtIDU7XG4gICAgdGhpcy50ZXh0UmFkaXVzID0gdGhpcy5yYWRpdXMgLSAyNjtcblxuICAgIHRoaXMuY3JlYXRlQ2xvY2tMaW5lcygpO1xuICAgIHRoaXMuY3JlYXRlQ2xvY2tUZXh0cygpO1xuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVDbG9ja0xpbmVzKCkge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgREVGQVVMVF9SQU5HRTsgaSsrKSB7XG4gICAgICBjb25zdCBjb3MgPSBNYXRoLmNvcygoKDIgKiBNYXRoLlBJKSAvIERFRkFVTFRfUkFOR0UpICogaSk7XG4gICAgICBjb25zdCBzaW4gPSBNYXRoLnNpbigoKDIgKiBNYXRoLlBJKSAvIERFRkFVTFRfUkFOR0UpICogaSk7XG4gICAgICB0aGlzLmNsb2NrTGluZXMucHVzaCh7XG4gICAgICAgIGlkOiBpLFxuICAgICAgICBzdHJva2VXaWR0aDogaSAlIDQgPT09IDAgPyAzIDogMSxcbiAgICAgICAgeDE6IGNvcyAqIHRoaXMuZmFjZVJhZGl1cyxcbiAgICAgICAgeTE6IHNpbiAqIHRoaXMuZmFjZVJhZGl1cyxcbiAgICAgICAgeDI6IGNvcyAqICh0aGlzLmZhY2VSYWRpdXMgLSA3KSxcbiAgICAgICAgeTI6IHNpbiAqICh0aGlzLmZhY2VSYWRpdXMgLSA3KVxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVDbG9ja1RleHRzKCkge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgREVGQVVMVF9USU1FX1JBTkdFOyBpKyspIHtcbiAgICAgIHRoaXMuY2xvY2tUZXh0cy5wdXNoKHtcbiAgICAgICAgaWQ6IGksXG4gICAgICAgIHg6XG4gICAgICAgICAgdGhpcy50ZXh0UmFkaXVzICpcbiAgICAgICAgICBNYXRoLmNvcygoKDIgKiBNYXRoLlBJKSAvIDEyKSAqIGkgLSBNYXRoLlBJIC8gMiArIE1hdGguUEkgLyA2KSxcbiAgICAgICAgeTpcbiAgICAgICAgICB0aGlzLnRleHRSYWRpdXMgKlxuICAgICAgICAgIE1hdGguc2luKCgoMiAqIE1hdGguUEkpIC8gMTIpICogaSAtIE1hdGguUEkgLyAyICsgTWF0aC5QSSAvIDYpXG4gICAgICB9KTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==