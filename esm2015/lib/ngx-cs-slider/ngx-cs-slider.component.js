/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { fromEvent, merge } from "rxjs";
import { interpolateHcl } from "d3-interpolate";
import { switchMap, takeUntil, throttleTime } from "rxjs/operators";
const /** @type {?} */ THROTTLE_DEFAULT = 50;
const /** @type {?} */ DEFAULT_PROPS = {
    segments: 6,
    strokeWidth: 40,
    radius: 145,
    gradientColorFrom: "#ff9800",
    gradientColorTo: "#ffcf00",
    bgCircleColor: "#171717",
    showClockFace: true,
    clockFaceColor: "#9d9d9d"
};
export class NgxCircularSliderComponent {
    constructor() {
        this.update = new EventEmitter();
        this.props = DEFAULT_PROPS;
        this.startAngle = 0;
        this.angleLength = 0;
    }
    /**
     * @param {?} evt
     * @return {?}
     */
    static extractMouseEventCoords(evt) {
        const /** @type {?} */ coords = evt instanceof MouseEvent
            ? {
                x: evt.clientX,
                y: evt.clientY
            }
            : {
                x: evt.changedTouches.item(0).clientX,
                y: evt.changedTouches.item(0).clientY
            };
        return coords;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.setCircleCenter();
        this.onUpdate();
        this.setObservables();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.props) {
            this.props = changes.props.firstChange
                ? Object.assign(DEFAULT_PROPS, changes.props.currentValue)
                : DEFAULT_PROPS;
        }
        this.onUpdate();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.closeStreams();
    }
    /**
     * @return {?}
     */
    onUpdate() {
        this.calcStartAndStop();
        this.createSegments();
        this.update.emit({
            startAngle: this.startAngle,
            angleLength: this.angleLength
        });
    }
    /**
     * @return {?}
     */
    setObservables() {
        const /** @type {?} */ mouseMove$ = merge(fromEvent(document, "mousemove"), fromEvent(document, "touchmove"));
        const /** @type {?} */ mouseUp$ = merge(fromEvent(document, "mouseup"), fromEvent(document, "touchend"));
        //     this.startSubscription = merge(
        //       fromEvent(this.startIcon.nativeElement, "touchstart"),
        //       fromEvent(this.startIcon.nativeElement, "mousedown")
        //     )
        //       .pipe(
        //         switchMap(_ =>
        //           mouseMove$.pipe(
        //             takeUntil(mouseUp$),
        //             throttleTime(THROTTLE_DEFAULT)
        //           )
        //         )
        //       )
        //       .subscribe((res: MouseEvent | TouchEvent) => {
        //         this.handleStartPan(res);
        //       });
        this.stopSubscription = merge(fromEvent(this.stopIcon.nativeElement, "touchstart"), fromEvent(this.stopIcon.nativeElement, "mousedown"))
            .pipe(switchMap(_ => mouseMove$.pipe(takeUntil(mouseUp$), throttleTime(THROTTLE_DEFAULT))))
            .subscribe((res) => {
            this.handleStopPan(res);
        });
    }
    /**
     * @return {?}
     */
    closeStreams() {
        //     if (this.startSubscription) {
        //       this.startSubscription.unsubscribe();
        //       this.startSubscription = null;
        //     }
        if (this.stopSubscription) {
            this.stopSubscription.unsubscribe();
            this.stopSubscription = null;
        }
    }
    /**
     * @param {?} evt
     * @return {?}
     */
    handleStartPan(evt) {
        const /** @type {?} */ coords = NgxCircularSliderComponent.extractMouseEventCoords(evt);
        this.setCircleCenter();
        const /** @type {?} */ currentAngleStop = (this.startAngle + this.angleLength) % (2 * Math.PI);
        let /** @type {?} */ newAngle = Math.atan2(coords.y - this.circleCenterY, coords.x - this.circleCenterX) +
            Math.PI / 2;
        if (newAngle < 0) {
            newAngle += 2 * Math.PI;
        }
        let /** @type {?} */ newAngleLength = currentAngleStop - newAngle;
        if (newAngleLength < 0) {
            newAngleLength += 2 * Math.PI;
        }
        this.startAngle = newAngle;
        this.angleLength = newAngleLength % (2 * Math.PI);
        this.onUpdate();
    }
    /**
     * @param {?} evt
     * @return {?}
     */
    handleStopPan(evt) {
        const /** @type {?} */ coords = NgxCircularSliderComponent.extractMouseEventCoords(evt);
        this.setCircleCenter();
        const /** @type {?} */ newAngle = Math.atan2(coords.y - this.circleCenterY, coords.x - this.circleCenterX) +
            Math.PI / 2;
        let /** @type {?} */ newAngleLength = (newAngle - this.startAngle) % (2 * Math.PI);
        if (newAngleLength < 0) {
            newAngleLength += 2 * Math.PI;
        }
        this.angleLength = newAngleLength;
        this.onUpdate();
    }
    /**
     * @return {?}
     */
    calcStartAndStop() {
        this.start = this.calculateArcCircle(0, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
        this.stop = this.calculateArcCircle(this.props.segments - 1, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
    }
    /**
     * @param {?} index
     * @param {?} segments
     * @param {?} gradientColorFrom
     * @param {?} gradientColorTo
     * @return {?}
     */
    calculateArcColor(index, segments, gradientColorFrom, gradientColorTo) {
        const /** @type {?} */ interpolate = interpolateHcl(gradientColorFrom, gradientColorTo);
        return {
            fromColor: interpolate(index / segments),
            toColor: interpolate((index + 1) / segments)
        };
    }
    /**
     * @param {?} indexInput
     * @param {?} segments
     * @param {?} radius
     * @param {?=} startAngleInput
     * @param {?=} angleLengthInput
     * @return {?}
     */
    calculateArcCircle(indexInput, segments, radius, startAngleInput = 0, angleLengthInput = 2 * Math.PI) {
        // Add 0.0001 to the possible angle so when start = stop angle, whole circle is drawn
        const /** @type {?} */ startAngle = startAngleInput % (2 * Math.PI);
        const /** @type {?} */ angleLength = angleLengthInput % (2 * Math.PI);
        const /** @type {?} */ index = indexInput + 1;
        const /** @type {?} */ fromAngle = (angleLength / segments) * (index - 1) + startAngle;
        const /** @type {?} */ toAngle = (angleLength / segments) * index + startAngle;
        const /** @type {?} */ fromX = radius * Math.sin(fromAngle);
        const /** @type {?} */ fromY = -radius * Math.cos(fromAngle);
        const /** @type {?} */ realToX = radius * Math.sin(toAngle);
        const /** @type {?} */ realToY = -radius * Math.cos(toAngle);
        // add 0.005 to start drawing a little bit earlier so segments stick together
        const /** @type {?} */ toX = radius * Math.sin(toAngle + 0.005);
        const /** @type {?} */ toY = -radius * Math.cos(toAngle + 0.005);
        return {
            fromX,
            fromY,
            toX,
            toY,
            realToX,
            realToY
        };
    }
    /**
     * @return {?}
     */
    createSegments() {
        this.segments = [];
        for (let /** @type {?} */ i = 0; i < this.props.segments; i++) {
            const /** @type {?} */ id = i;
            const /** @type {?} */ colors = this.calculateArcColor(id, this.props.segments, this.props.gradientColorFrom, this.props.gradientColorTo);
            const /** @type {?} */ arcs = this.calculateArcCircle(id, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
            this.segments.push({
                id: id,
                d: `M ${arcs.fromX.toFixed(2)} ${arcs.fromY.toFixed(2)} A ${this.props.radius} ${this.props.radius} 
        0 0 1 ${arcs.toX.toFixed(2)} ${arcs.toY.toFixed(2)}`,
                colors: Object.assign({}, colors),
                arcs: Object.assign({}, arcs)
            });
        }
    }
    /**
     * @return {?}
     */
    setCircleCenter() {
        // todo: nicer solution to use document.body?
        const /** @type {?} */ bodyRect = document.body.getBoundingClientRect();
        const /** @type {?} */ elemRect = this.circle.nativeElement.getBoundingClientRect();
        const /** @type {?} */ px = elemRect.left - bodyRect.left;
        const /** @type {?} */ py = elemRect.top - bodyRect.top;
        const /** @type {?} */ halfOfContainer = this.getContainerWidth() / 2;
        this.circleCenterX = px + halfOfContainer;
        this.circleCenterY = py + halfOfContainer;
    }
    /**
     * @return {?}
     */
    getContainerWidth() {
        const { strokeWidth, radius } = this.props;
        return strokeWidth + radius * 2 + 2;
    }
    /**
     * @param {?} index
     * @return {?}
     */
    getGradientId(index) {
        return `gradient${index}`;
    }
    /**
     * @param {?} index
     * @return {?}
     */
    getGradientUrl(index) {
        return 'url(' + window.location + '#gradient' + index + ')';
    }
    /**
     * @return {?}
     */
    getTranslate() {
        return ` translate(
  ${this.props.strokeWidth / 2 + this.props.radius + 1},
  ${this.props.strokeWidth / 2 + this.props.radius + 1} )`;
    }
    /**
     * @param {?} x
     * @param {?} y
     * @return {?}
     */
    getTranslateFrom(x, y) {
        return ` translate(${x}, ${y})`;
    }
}
NgxCircularSliderComponent.decorators = [
    { type: Component, args: [{
                selector: "ngx-cs-slider",
                templateUrl: "./ngx-cs-slider.component.html",
                styleUrls: ["./ngx-cs-slider.component.scss"]
            },] },
];
/** @nocollapse */
NgxCircularSliderComponent.ctorParameters = () => [];
NgxCircularSliderComponent.propDecorators = {
    props: [{ type: Input }],
    startAngle: [{ type: Input }],
    angleLength: [{ type: Input }],
    update: [{ type: Output }],
    circle: [{ type: ViewChild, args: ["circle",] }],
    stopIcon: [{ type: ViewChild, args: ["stopIcon",] }],
    startIcon: [{ type: ViewChild, args: ["startIcon",] }]
};
function NgxCircularSliderComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    NgxCircularSliderComponent.prototype.props;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.startAngle;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.angleLength;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.update;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.segments;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.start;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.stop;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.stopSubscription;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.circleCenterX;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.circleCenterY;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.circle;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.stopIcon;
    /** @type {?} */
    NgxCircularSliderComponent.prototype.startIcon;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNzLXNsaWRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY2lyY3VsYXItc2xpZGVyLyIsInNvdXJjZXMiOlsibGliL25neC1jcy1zbGlkZXIvbmd4LWNzLXNsaWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsVUFBVSxFQUNWLFlBQVksRUFDWixLQUFLLEVBSUwsTUFBTSxFQUNOLFNBQVMsRUFDVixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBVWhELE9BQU8sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXBFLHVCQUFNLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztBQUM1Qix1QkFBTSxhQUFhLEdBQVc7SUFDNUIsUUFBUSxFQUFFLENBQUM7SUFDWCxXQUFXLEVBQUUsRUFBRTtJQUNmLE1BQU0sRUFBRSxHQUFHO0lBQ1gsaUJBQWlCLEVBQUUsU0FBUztJQUM1QixlQUFlLEVBQUUsU0FBUztJQUMxQixhQUFhLEVBQUUsU0FBUztJQUN4QixhQUFhLEVBQUUsSUFBSTtJQUNuQixjQUFjLEVBQUUsU0FBUztDQUMxQixDQUFDO0FBT0YsTUFBTTtJQStCSjtzQkExQjBDLElBQUksWUFBWSxFQUFXO1FBMkJuRSxJQUFJLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUNwQixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztLQUN0Qjs7Ozs7SUFsQk8sTUFBTSxDQUFDLHVCQUF1QixDQUFDLEdBQTRCO1FBQ2pFLHVCQUFNLE1BQU0sR0FDVixHQUFHLFlBQVksVUFBVTtZQUN2QixDQUFDLENBQUM7Z0JBQ0UsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxPQUFPO2dCQUNkLENBQUMsRUFBRSxHQUFHLENBQUMsT0FBTzthQUNmO1lBQ0gsQ0FBQyxDQUFDO2dCQUNFLENBQUMsRUFBRSxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO2dCQUNyQyxDQUFDLEVBQUUsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTzthQUN0QyxDQUFDO1FBQ1IsTUFBTSxDQUFDLE1BQU0sQ0FBQzs7Ozs7SUFTaEIsUUFBUTtRQUNOLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0tBQ3ZCOzs7OztJQUVELFdBQVcsQ0FBQyxPQUF1QjtRQUNqQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsV0FBVztnQkFDcEMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDO2dCQUMxRCxDQUFDLENBQUMsYUFBYSxDQUFDO1NBQ25CO1FBQ0QsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0tBQ2pCOzs7O0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztLQUNyQjs7OztJQUVPLFFBQVE7UUFDZCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDZixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7WUFDM0IsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO1NBQzlCLENBQUMsQ0FBQzs7Ozs7SUFHRyxjQUFjO1FBQ3BCLHVCQUFNLFVBQVUsR0FBRyxLQUFLLENBQ3RCLFNBQVMsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLEVBQ2hDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQ2pDLENBQUM7UUFDRix1QkFBTSxRQUFRLEdBQUcsS0FBSyxDQUNwQixTQUFTLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxFQUM5QixTQUFTLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUNoQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7O1FBa0JGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQzNCLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsRUFDcEQsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUNwRDthQUNFLElBQUksQ0FDSCxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FDWixVQUFVLENBQUMsSUFBSSxDQUNiLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFDbkIsWUFBWSxDQUFDLGdCQUFnQixDQUFDLENBQy9CLENBQ0YsQ0FDRjthQUNBLFNBQVMsQ0FBQyxDQUFDLEdBQTRCLEVBQUUsRUFBRTtZQUMxQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3pCLENBQUMsQ0FBQzs7Ozs7SUFHQyxZQUFZOzs7OztRQUtsQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1NBQzlCOzs7Ozs7SUFHSyxjQUFjLENBQUMsR0FBNEI7UUFDakQsdUJBQU0sTUFBTSxHQUFHLDBCQUEwQixDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXZFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2Qix1QkFBTSxnQkFBZ0IsR0FDcEIsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdkQscUJBQUksUUFBUSxHQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUN4RSxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVkLEVBQUUsQ0FBQyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLFFBQVEsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztTQUN6QjtRQUVELHFCQUFJLGNBQWMsR0FBRyxnQkFBZ0IsR0FBRyxRQUFRLENBQUM7UUFDakQsRUFBRSxDQUFDLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkIsY0FBYyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1NBQy9CO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxjQUFjLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWxELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzs7Ozs7O0lBR1YsYUFBYSxDQUFDLEdBQTRCO1FBQ2hELHVCQUFNLE1BQU0sR0FBRywwQkFBMEIsQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsdUJBQU0sUUFBUSxHQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUN4RSxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNkLHFCQUFJLGNBQWMsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWxFLEVBQUUsQ0FBQyxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLGNBQWMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztTQUMvQjtRQUVELElBQUksQ0FBQyxXQUFXLEdBQUcsY0FBYyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzs7Ozs7SUFHVixnQkFBZ0I7UUFDdEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQ2xDLENBQUMsRUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQ2pCLElBQUksQ0FBQyxVQUFVLEVBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztRQUNGLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUNqQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxDQUFDLEVBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFDakIsSUFBSSxDQUFDLFVBQVUsRUFDZixJQUFJLENBQUMsV0FBVyxDQUNqQixDQUFDOzs7Ozs7Ozs7SUFHSSxpQkFBaUIsQ0FDdkIsS0FBSyxFQUNMLFFBQVEsRUFDUixpQkFBaUIsRUFDakIsZUFBZTtRQUVmLHVCQUFNLFdBQVcsR0FBRyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFFdkUsTUFBTSxDQUFDO1lBQ0wsU0FBUyxFQUFFLFdBQVcsQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO1lBQ3hDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDO1NBQzdDLENBQUM7Ozs7Ozs7Ozs7SUFHSSxrQkFBa0IsQ0FDeEIsVUFBVSxFQUNWLFFBQVEsRUFDUixNQUFNLEVBQ04sZUFBZSxHQUFHLENBQUMsRUFDbkIsZ0JBQWdCLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFOztRQUc5Qix1QkFBTSxVQUFVLEdBQUcsZUFBZSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuRCx1QkFBTSxXQUFXLEdBQUcsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3JELHVCQUFNLEtBQUssR0FBRyxVQUFVLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLHVCQUFNLFNBQVMsR0FBRyxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUM7UUFDdEUsdUJBQU0sT0FBTyxHQUFHLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxHQUFHLEtBQUssR0FBRyxVQUFVLENBQUM7UUFDOUQsdUJBQU0sS0FBSyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNDLHVCQUFNLEtBQUssR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVDLHVCQUFNLE9BQU8sR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzQyx1QkFBTSxPQUFPLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7UUFHNUMsdUJBQU0sR0FBRyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsQ0FBQztRQUMvQyx1QkFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFFaEQsTUFBTSxDQUFDO1lBQ0wsS0FBSztZQUNMLEtBQUs7WUFDTCxHQUFHO1lBQ0gsR0FBRztZQUNILE9BQU87WUFDUCxPQUFPO1NBQ1IsQ0FBQzs7Ozs7SUFHSSxjQUFjO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ25CLEdBQUcsQ0FBQyxDQUFDLHFCQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDN0MsdUJBQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNiLHVCQUFNLE1BQU0sR0FBVyxJQUFJLENBQUMsaUJBQWlCLENBQzNDLEVBQUUsRUFDRixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQzNCLENBQUM7WUFDRix1QkFBTSxJQUFJLEdBQVMsSUFBSSxDQUFDLGtCQUFrQixDQUN4QyxFQUFFLEVBQ0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUNqQixJQUFJLENBQUMsVUFBVSxFQUNmLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7WUFFRixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDakIsRUFBRSxFQUFFLEVBQUU7Z0JBQ04sQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQ3BELElBQUksQ0FBQyxLQUFLLENBQUMsTUFDYixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTTtnQkFDYixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDcEQsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQztnQkFDakMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDSjs7Ozs7SUFHSyxlQUFlOztRQUVyQix1QkFBTSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ3ZELHVCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ25FLHVCQUFNLEVBQUUsR0FBRyxRQUFRLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDekMsdUJBQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUN2Qyx1QkFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxHQUFHLGVBQWUsQ0FBQztRQUMxQyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsR0FBRyxlQUFlLENBQUM7Ozs7O0lBR3JDLGlCQUFpQjtRQUN0QixNQUFNLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDM0MsTUFBTSxDQUFDLFdBQVcsR0FBRyxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQzs7Ozs7O0lBRy9CLGFBQWEsQ0FBQyxLQUFLO1FBQ3hCLE1BQU0sQ0FBQyxXQUFXLEtBQUssRUFBRSxDQUFDOzs7Ozs7SUFHckIsY0FBYyxDQUFDLEtBQUs7UUFDekIsTUFBTSxDQUFDLE1BQU0sR0FBQyxNQUFNLENBQUMsUUFBUSxHQUFDLFdBQVcsR0FBQyxLQUFLLEdBQUMsR0FBRyxDQUFDOzs7OztJQUcvQyxZQUFZO1FBQ2pCLE1BQU0sQ0FBQztJQUNQLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDO0lBQ2xELElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQzs7Ozs7OztJQUdsRCxnQkFBZ0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxQixNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7Ozs7WUFqU25DLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsV0FBVyxFQUFFLGdDQUFnQztnQkFDN0MsU0FBUyxFQUFFLENBQUMsZ0NBQWdDLENBQUM7YUFDOUM7Ozs7O29CQUdFLEtBQUs7eUJBQ0wsS0FBSzswQkFDTCxLQUFLO3FCQUNMLE1BQU07cUJBUU4sU0FBUyxTQUFDLFFBQVE7dUJBQ2xCLFNBQVMsU0FBQyxVQUFVO3dCQUNwQixTQUFTLFNBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIEVsZW1lbnRSZWYsXHJcbiAgRXZlbnRFbWl0dGVyLFxyXG4gIElucHV0LFxyXG4gIE9uQ2hhbmdlcyxcclxuICBPbkRlc3Ryb3ksXHJcbiAgT25Jbml0LFxyXG4gIE91dHB1dCxcclxuICBWaWV3Q2hpbGRcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBmcm9tRXZlbnQsIG1lcmdlLCBTdWJzY3JpcHRpb24gfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBpbnRlcnBvbGF0ZUhjbCB9IGZyb20gXCJkMy1pbnRlcnBvbGF0ZVwiO1xyXG5pbXBvcnQge1xyXG4gIElBcmMsXHJcbiAgSUNvbG9yLFxyXG4gIElDb29yZHMsXHJcbiAgSU91dHB1dCxcclxuICBJUHJvcHMsXHJcbiAgSVNlZ21lbnQsXHJcbiAgSVNsaWRlckNoYW5nZXNcclxufSBmcm9tIFwiLi4vaW50ZXJmYWNlc1wiO1xyXG5pbXBvcnQgeyBzd2l0Y2hNYXAsIHRha2VVbnRpbCwgdGhyb3R0bGVUaW1lIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcblxyXG5jb25zdCBUSFJPVFRMRV9ERUZBVUxUID0gNTA7XHJcbmNvbnN0IERFRkFVTFRfUFJPUFM6IElQcm9wcyA9IHtcclxuICBzZWdtZW50czogNixcclxuICBzdHJva2VXaWR0aDogNDAsXHJcbiAgcmFkaXVzOiAxNDUsXHJcbiAgZ3JhZGllbnRDb2xvckZyb206IFwiI2ZmOTgwMFwiLFxyXG4gIGdyYWRpZW50Q29sb3JUbzogXCIjZmZjZjAwXCIsXHJcbiAgYmdDaXJjbGVDb2xvcjogXCIjMTcxNzE3XCIsXHJcbiAgc2hvd0Nsb2NrRmFjZTogdHJ1ZSxcclxuICBjbG9ja0ZhY2VDb2xvcjogXCIjOWQ5ZDlkXCJcclxufTtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm5neC1jcy1zbGlkZXJcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL25neC1jcy1zbGlkZXIuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vbmd4LWNzLXNsaWRlci5jb21wb25lbnQuc2Nzc1wiXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd4Q2lyY3VsYXJTbGlkZXJDb21wb25lbnRcclxuICBpbXBsZW1lbnRzIE9uQ2hhbmdlcywgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gIEBJbnB1dCgpIHByb3BzOiBJUHJvcHM7XHJcbiAgQElucHV0KCkgc3RhcnRBbmdsZTogbnVtYmVyO1xyXG4gIEBJbnB1dCgpIGFuZ2xlTGVuZ3RoOiBudW1iZXI7XHJcbiAgQE91dHB1dCgpIHVwZGF0ZTogRXZlbnRFbWl0dGVyPElPdXRwdXQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxJT3V0cHV0PigpO1xyXG4gIHB1YmxpYyBzZWdtZW50czogSVNlZ21lbnRbXTtcclxuICBwdWJsaWMgc3RhcnQ6IElBcmM7XHJcbiAgcHVibGljIHN0b3A6IElBcmM7XHJcbi8vICAgcHJpdmF0ZSBzdGFydFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIHByaXZhdGUgc3RvcFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIHByaXZhdGUgY2lyY2xlQ2VudGVyWDogbnVtYmVyO1xyXG4gIHByaXZhdGUgY2lyY2xlQ2VudGVyWTogbnVtYmVyO1xyXG4gIEBWaWV3Q2hpbGQoXCJjaXJjbGVcIikgcHJpdmF0ZSBjaXJjbGU6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcInN0b3BJY29uXCIpIHByaXZhdGUgc3RvcEljb246IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcInN0YXJ0SWNvblwiKSBwcml2YXRlIHN0YXJ0SWNvbjogRWxlbWVudFJlZjtcclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgZXh0cmFjdE1vdXNlRXZlbnRDb29yZHMoZXZ0OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkge1xyXG4gICAgY29uc3QgY29vcmRzOiBJQ29vcmRzID1cclxuICAgICAgZXZ0IGluc3RhbmNlb2YgTW91c2VFdmVudFxyXG4gICAgICAgID8ge1xyXG4gICAgICAgICAgICB4OiBldnQuY2xpZW50WCxcclxuICAgICAgICAgICAgeTogZXZ0LmNsaWVudFlcclxuICAgICAgICAgIH1cclxuICAgICAgICA6IHtcclxuICAgICAgICAgICAgeDogZXZ0LmNoYW5nZWRUb3VjaGVzLml0ZW0oMCkuY2xpZW50WCxcclxuICAgICAgICAgICAgeTogZXZ0LmNoYW5nZWRUb3VjaGVzLml0ZW0oMCkuY2xpZW50WVxyXG4gICAgICAgICAgfTtcclxuICAgIHJldHVybiBjb29yZHM7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMucHJvcHMgPSBERUZBVUxUX1BST1BTO1xyXG4gICAgdGhpcy5zdGFydEFuZ2xlID0gMDtcclxuICAgIHRoaXMuYW5nbGVMZW5ndGggPSAwO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnNldENpcmNsZUNlbnRlcigpO1xyXG4gICAgdGhpcy5vblVwZGF0ZSgpO1xyXG4gICAgdGhpcy5zZXRPYnNlcnZhYmxlcygpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogSVNsaWRlckNoYW5nZXMpIHtcclxuICAgIGlmIChjaGFuZ2VzLnByb3BzKSB7XHJcbiAgICAgIHRoaXMucHJvcHMgPSBjaGFuZ2VzLnByb3BzLmZpcnN0Q2hhbmdlXHJcbiAgICAgICAgPyBPYmplY3QuYXNzaWduKERFRkFVTFRfUFJPUFMsIGNoYW5nZXMucHJvcHMuY3VycmVudFZhbHVlKVxyXG4gICAgICAgIDogREVGQVVMVF9QUk9QUztcclxuICAgIH1cclxuICAgIHRoaXMub25VcGRhdGUoKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5jbG9zZVN0cmVhbXMoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgb25VcGRhdGUoKSB7XHJcbiAgICB0aGlzLmNhbGNTdGFydEFuZFN0b3AoKTtcclxuICAgIHRoaXMuY3JlYXRlU2VnbWVudHMoKTtcclxuICAgIHRoaXMudXBkYXRlLmVtaXQoe1xyXG4gICAgICBzdGFydEFuZ2xlOiB0aGlzLnN0YXJ0QW5nbGUsXHJcbiAgICAgIGFuZ2xlTGVuZ3RoOiB0aGlzLmFuZ2xlTGVuZ3RoXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc2V0T2JzZXJ2YWJsZXMoKSB7XHJcbiAgICBjb25zdCBtb3VzZU1vdmUkID0gbWVyZ2UoXHJcbiAgICAgIGZyb21FdmVudChkb2N1bWVudCwgXCJtb3VzZW1vdmVcIiksXHJcbiAgICAgIGZyb21FdmVudChkb2N1bWVudCwgXCJ0b3VjaG1vdmVcIilcclxuICAgICk7XHJcbiAgICBjb25zdCBtb3VzZVVwJCA9IG1lcmdlKFxyXG4gICAgICBmcm9tRXZlbnQoZG9jdW1lbnQsIFwibW91c2V1cFwiKSxcclxuICAgICAgZnJvbUV2ZW50KGRvY3VtZW50LCBcInRvdWNoZW5kXCIpXHJcbiAgICApO1xyXG5cclxuLy8gICAgIHRoaXMuc3RhcnRTdWJzY3JpcHRpb24gPSBtZXJnZShcclxuLy8gICAgICAgZnJvbUV2ZW50KHRoaXMuc3RhcnRJY29uLm5hdGl2ZUVsZW1lbnQsIFwidG91Y2hzdGFydFwiKSxcclxuLy8gICAgICAgZnJvbUV2ZW50KHRoaXMuc3RhcnRJY29uLm5hdGl2ZUVsZW1lbnQsIFwibW91c2Vkb3duXCIpXHJcbi8vICAgICApXHJcbi8vICAgICAgIC5waXBlKFxyXG4vLyAgICAgICAgIHN3aXRjaE1hcChfID0+XHJcbi8vICAgICAgICAgICBtb3VzZU1vdmUkLnBpcGUoXHJcbi8vICAgICAgICAgICAgIHRha2VVbnRpbChtb3VzZVVwJCksXHJcbi8vICAgICAgICAgICAgIHRocm90dGxlVGltZShUSFJPVFRMRV9ERUZBVUxUKVxyXG4vLyAgICAgICAgICAgKVxyXG4vLyAgICAgICAgIClcclxuLy8gICAgICAgKVxyXG4vLyAgICAgICAuc3Vic2NyaWJlKChyZXM6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSA9PiB7XHJcbi8vICAgICAgICAgdGhpcy5oYW5kbGVTdGFydFBhbihyZXMpO1xyXG4vLyAgICAgICB9KTtcclxuXHJcbiAgICB0aGlzLnN0b3BTdWJzY3JpcHRpb24gPSBtZXJnZShcclxuICAgICAgZnJvbUV2ZW50KHRoaXMuc3RvcEljb24ubmF0aXZlRWxlbWVudCwgXCJ0b3VjaHN0YXJ0XCIpLFxyXG4gICAgICBmcm9tRXZlbnQodGhpcy5zdG9wSWNvbi5uYXRpdmVFbGVtZW50LCBcIm1vdXNlZG93blwiKVxyXG4gICAgKVxyXG4gICAgICAucGlwZShcclxuICAgICAgICBzd2l0Y2hNYXAoXyA9PlxyXG4gICAgICAgICAgbW91c2VNb3ZlJC5waXBlKFxyXG4gICAgICAgICAgICB0YWtlVW50aWwobW91c2VVcCQpLFxyXG4gICAgICAgICAgICB0aHJvdHRsZVRpbWUoVEhST1RUTEVfREVGQVVMVClcclxuICAgICAgICAgIClcclxuICAgICAgICApXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSgocmVzOiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkgPT4ge1xyXG4gICAgICAgIHRoaXMuaGFuZGxlU3RvcFBhbihyZXMpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY2xvc2VTdHJlYW1zKCkge1xyXG4vLyAgICAgaWYgKHRoaXMuc3RhcnRTdWJzY3JpcHRpb24pIHtcclxuLy8gICAgICAgdGhpcy5zdGFydFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4vLyAgICAgICB0aGlzLnN0YXJ0U3Vic2NyaXB0aW9uID0gbnVsbDtcclxuLy8gICAgIH1cclxuICAgIGlmICh0aGlzLnN0b3BTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5zdG9wU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgIHRoaXMuc3RvcFN1YnNjcmlwdGlvbiA9IG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZVN0YXJ0UGFuKGV2dDogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQpIHtcclxuICAgIGNvbnN0IGNvb3JkcyA9IE5neENpcmN1bGFyU2xpZGVyQ29tcG9uZW50LmV4dHJhY3RNb3VzZUV2ZW50Q29vcmRzKGV2dCk7XHJcblxyXG4gICAgdGhpcy5zZXRDaXJjbGVDZW50ZXIoKTtcclxuICAgIGNvbnN0IGN1cnJlbnRBbmdsZVN0b3AgPVxyXG4gICAgICAodGhpcy5zdGFydEFuZ2xlICsgdGhpcy5hbmdsZUxlbmd0aCkgJSAoMiAqIE1hdGguUEkpO1xyXG4gICAgbGV0IG5ld0FuZ2xlID1cclxuICAgICAgTWF0aC5hdGFuMihjb29yZHMueSAtIHRoaXMuY2lyY2xlQ2VudGVyWSwgY29vcmRzLnggLSB0aGlzLmNpcmNsZUNlbnRlclgpICtcclxuICAgICAgTWF0aC5QSSAvIDI7XHJcblxyXG4gICAgaWYgKG5ld0FuZ2xlIDwgMCkge1xyXG4gICAgICBuZXdBbmdsZSArPSAyICogTWF0aC5QSTtcclxuICAgIH1cclxuXHJcbiAgICBsZXQgbmV3QW5nbGVMZW5ndGggPSBjdXJyZW50QW5nbGVTdG9wIC0gbmV3QW5nbGU7XHJcbiAgICBpZiAobmV3QW5nbGVMZW5ndGggPCAwKSB7XHJcbiAgICAgIG5ld0FuZ2xlTGVuZ3RoICs9IDIgKiBNYXRoLlBJO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc3RhcnRBbmdsZSA9IG5ld0FuZ2xlO1xyXG4gICAgdGhpcy5hbmdsZUxlbmd0aCA9IG5ld0FuZ2xlTGVuZ3RoICUgKDIgKiBNYXRoLlBJKTtcclxuXHJcbiAgICB0aGlzLm9uVXBkYXRlKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGhhbmRsZVN0b3BQYW4oZXZ0OiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkge1xyXG4gICAgY29uc3QgY29vcmRzID0gTmd4Q2lyY3VsYXJTbGlkZXJDb21wb25lbnQuZXh0cmFjdE1vdXNlRXZlbnRDb29yZHMoZXZ0KTtcclxuICAgIHRoaXMuc2V0Q2lyY2xlQ2VudGVyKCk7XHJcbiAgICBjb25zdCBuZXdBbmdsZSA9XHJcbiAgICAgIE1hdGguYXRhbjIoY29vcmRzLnkgLSB0aGlzLmNpcmNsZUNlbnRlclksIGNvb3Jkcy54IC0gdGhpcy5jaXJjbGVDZW50ZXJYKSArXHJcbiAgICAgIE1hdGguUEkgLyAyO1xyXG4gICAgbGV0IG5ld0FuZ2xlTGVuZ3RoID0gKG5ld0FuZ2xlIC0gdGhpcy5zdGFydEFuZ2xlKSAlICgyICogTWF0aC5QSSk7XHJcblxyXG4gICAgaWYgKG5ld0FuZ2xlTGVuZ3RoIDwgMCkge1xyXG4gICAgICBuZXdBbmdsZUxlbmd0aCArPSAyICogTWF0aC5QSTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmFuZ2xlTGVuZ3RoID0gbmV3QW5nbGVMZW5ndGg7XHJcbiAgICB0aGlzLm9uVXBkYXRlKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNhbGNTdGFydEFuZFN0b3AoKSB7XHJcbiAgICB0aGlzLnN0YXJ0ID0gdGhpcy5jYWxjdWxhdGVBcmNDaXJjbGUoXHJcbiAgICAgIDAsXHJcbiAgICAgIHRoaXMucHJvcHMuc2VnbWVudHMsXHJcbiAgICAgIHRoaXMucHJvcHMucmFkaXVzLFxyXG4gICAgICB0aGlzLnN0YXJ0QW5nbGUsXHJcbiAgICAgIHRoaXMuYW5nbGVMZW5ndGhcclxuICAgICk7XHJcbiAgICB0aGlzLnN0b3AgPSB0aGlzLmNhbGN1bGF0ZUFyY0NpcmNsZShcclxuICAgICAgdGhpcy5wcm9wcy5zZWdtZW50cyAtIDEsXHJcbiAgICAgIHRoaXMucHJvcHMuc2VnbWVudHMsXHJcbiAgICAgIHRoaXMucHJvcHMucmFkaXVzLFxyXG4gICAgICB0aGlzLnN0YXJ0QW5nbGUsXHJcbiAgICAgIHRoaXMuYW5nbGVMZW5ndGhcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNhbGN1bGF0ZUFyY0NvbG9yKFxyXG4gICAgaW5kZXgsXHJcbiAgICBzZWdtZW50cyxcclxuICAgIGdyYWRpZW50Q29sb3JGcm9tLFxyXG4gICAgZ3JhZGllbnRDb2xvclRvXHJcbiAgKSB7XHJcbiAgICBjb25zdCBpbnRlcnBvbGF0ZSA9IGludGVycG9sYXRlSGNsKGdyYWRpZW50Q29sb3JGcm9tLCBncmFkaWVudENvbG9yVG8pO1xyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZyb21Db2xvcjogaW50ZXJwb2xhdGUoaW5kZXggLyBzZWdtZW50cyksXHJcbiAgICAgIHRvQ29sb3I6IGludGVycG9sYXRlKChpbmRleCArIDEpIC8gc2VnbWVudHMpXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjYWxjdWxhdGVBcmNDaXJjbGUoXHJcbiAgICBpbmRleElucHV0LFxyXG4gICAgc2VnbWVudHMsXHJcbiAgICByYWRpdXMsXHJcbiAgICBzdGFydEFuZ2xlSW5wdXQgPSAwLFxyXG4gICAgYW5nbGVMZW5ndGhJbnB1dCA9IDIgKiBNYXRoLlBJXHJcbiAgKSB7XHJcbiAgICAvLyBBZGQgMC4wMDAxIHRvIHRoZSBwb3NzaWJsZSBhbmdsZSBzbyB3aGVuIHN0YXJ0ID0gc3RvcCBhbmdsZSwgd2hvbGUgY2lyY2xlIGlzIGRyYXduXHJcbiAgICBjb25zdCBzdGFydEFuZ2xlID0gc3RhcnRBbmdsZUlucHV0ICUgKDIgKiBNYXRoLlBJKTtcclxuICAgIGNvbnN0IGFuZ2xlTGVuZ3RoID0gYW5nbGVMZW5ndGhJbnB1dCAlICgyICogTWF0aC5QSSk7XHJcbiAgICBjb25zdCBpbmRleCA9IGluZGV4SW5wdXQgKyAxO1xyXG4gICAgY29uc3QgZnJvbUFuZ2xlID0gKGFuZ2xlTGVuZ3RoIC8gc2VnbWVudHMpICogKGluZGV4IC0gMSkgKyBzdGFydEFuZ2xlO1xyXG4gICAgY29uc3QgdG9BbmdsZSA9IChhbmdsZUxlbmd0aCAvIHNlZ21lbnRzKSAqIGluZGV4ICsgc3RhcnRBbmdsZTtcclxuICAgIGNvbnN0IGZyb21YID0gcmFkaXVzICogTWF0aC5zaW4oZnJvbUFuZ2xlKTtcclxuICAgIGNvbnN0IGZyb21ZID0gLXJhZGl1cyAqIE1hdGguY29zKGZyb21BbmdsZSk7XHJcbiAgICBjb25zdCByZWFsVG9YID0gcmFkaXVzICogTWF0aC5zaW4odG9BbmdsZSk7XHJcbiAgICBjb25zdCByZWFsVG9ZID0gLXJhZGl1cyAqIE1hdGguY29zKHRvQW5nbGUpO1xyXG5cclxuICAgIC8vIGFkZCAwLjAwNSB0byBzdGFydCBkcmF3aW5nIGEgbGl0dGxlIGJpdCBlYXJsaWVyIHNvIHNlZ21lbnRzIHN0aWNrIHRvZ2V0aGVyXHJcbiAgICBjb25zdCB0b1ggPSByYWRpdXMgKiBNYXRoLnNpbih0b0FuZ2xlICsgMC4wMDUpO1xyXG4gICAgY29uc3QgdG9ZID0gLXJhZGl1cyAqIE1hdGguY29zKHRvQW5nbGUgKyAwLjAwNSk7XHJcblxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZnJvbVgsXHJcbiAgICAgIGZyb21ZLFxyXG4gICAgICB0b1gsXHJcbiAgICAgIHRvWSxcclxuICAgICAgcmVhbFRvWCxcclxuICAgICAgcmVhbFRvWVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY3JlYXRlU2VnbWVudHMoKSB7XHJcbiAgICB0aGlzLnNlZ21lbnRzID0gW107XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucHJvcHMuc2VnbWVudHM7IGkrKykge1xyXG4gICAgICBjb25zdCBpZCA9IGk7XHJcbiAgICAgIGNvbnN0IGNvbG9yczogSUNvbG9yID0gdGhpcy5jYWxjdWxhdGVBcmNDb2xvcihcclxuICAgICAgICBpZCxcclxuICAgICAgICB0aGlzLnByb3BzLnNlZ21lbnRzLFxyXG4gICAgICAgIHRoaXMucHJvcHMuZ3JhZGllbnRDb2xvckZyb20sXHJcbiAgICAgICAgdGhpcy5wcm9wcy5ncmFkaWVudENvbG9yVG9cclxuICAgICAgKTtcclxuICAgICAgY29uc3QgYXJjczogSUFyYyA9IHRoaXMuY2FsY3VsYXRlQXJjQ2lyY2xlKFxyXG4gICAgICAgIGlkLFxyXG4gICAgICAgIHRoaXMucHJvcHMuc2VnbWVudHMsXHJcbiAgICAgICAgdGhpcy5wcm9wcy5yYWRpdXMsXHJcbiAgICAgICAgdGhpcy5zdGFydEFuZ2xlLFxyXG4gICAgICAgIHRoaXMuYW5nbGVMZW5ndGhcclxuICAgICAgKTtcclxuXHJcbiAgICAgIHRoaXMuc2VnbWVudHMucHVzaCh7XHJcbiAgICAgICAgaWQ6IGlkLFxyXG4gICAgICAgIGQ6IGBNICR7YXJjcy5mcm9tWC50b0ZpeGVkKDIpfSAke2FyY3MuZnJvbVkudG9GaXhlZCgyKX0gQSAke1xyXG4gICAgICAgICAgdGhpcy5wcm9wcy5yYWRpdXNcclxuICAgICAgICB9ICR7dGhpcy5wcm9wcy5yYWRpdXN9IFxyXG4gICAgICAgIDAgMCAxICR7YXJjcy50b1gudG9GaXhlZCgyKX0gJHthcmNzLnRvWS50b0ZpeGVkKDIpfWAsXHJcbiAgICAgICAgY29sb3JzOiBPYmplY3QuYXNzaWduKHt9LCBjb2xvcnMpLFxyXG4gICAgICAgIGFyY3M6IE9iamVjdC5hc3NpZ24oe30sIGFyY3MpXHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzZXRDaXJjbGVDZW50ZXIoKSB7XHJcbiAgICAvLyB0b2RvOiBuaWNlciBzb2x1dGlvbiB0byB1c2UgZG9jdW1lbnQuYm9keT9cclxuICAgIGNvbnN0IGJvZHlSZWN0ID0gZG9jdW1lbnQuYm9keS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgIGNvbnN0IGVsZW1SZWN0ID0gdGhpcy5jaXJjbGUubmF0aXZlRWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgIGNvbnN0IHB4ID0gZWxlbVJlY3QubGVmdCAtIGJvZHlSZWN0LmxlZnQ7XHJcbiAgICBjb25zdCBweSA9IGVsZW1SZWN0LnRvcCAtIGJvZHlSZWN0LnRvcDtcclxuICAgIGNvbnN0IGhhbGZPZkNvbnRhaW5lciA9IHRoaXMuZ2V0Q29udGFpbmVyV2lkdGgoKSAvIDI7XHJcbiAgICB0aGlzLmNpcmNsZUNlbnRlclggPSBweCArIGhhbGZPZkNvbnRhaW5lcjtcclxuICAgIHRoaXMuY2lyY2xlQ2VudGVyWSA9IHB5ICsgaGFsZk9mQ29udGFpbmVyO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldENvbnRhaW5lcldpZHRoKCkge1xyXG4gICAgY29uc3QgeyBzdHJva2VXaWR0aCwgcmFkaXVzIH0gPSB0aGlzLnByb3BzO1xyXG4gICAgcmV0dXJuIHN0cm9rZVdpZHRoICsgcmFkaXVzICogMiArIDI7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0R3JhZGllbnRJZChpbmRleCkge1xyXG4gICAgcmV0dXJuIGBncmFkaWVudCR7aW5kZXh9YDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRHcmFkaWVudFVybChpbmRleCkge1xyXG4gICAgcmV0dXJuICd1cmwoJyt3aW5kb3cubG9jYXRpb24rJyNncmFkaWVudCcraW5kZXgrJyknO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFRyYW5zbGF0ZSgpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIGAgdHJhbnNsYXRlKFxyXG4gICR7dGhpcy5wcm9wcy5zdHJva2VXaWR0aCAvIDIgKyB0aGlzLnByb3BzLnJhZGl1cyArIDF9LFxyXG4gICR7dGhpcy5wcm9wcy5zdHJva2VXaWR0aCAvIDIgKyB0aGlzLnByb3BzLnJhZGl1cyArIDF9IClgO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFRyYW5zbGF0ZUZyb20oeCwgeSk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gYCB0cmFuc2xhdGUoJHt4fSwgJHt5fSlgO1xyXG4gIH1cclxufVxyXG4iXX0=