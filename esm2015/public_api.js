/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/*
 * Public API Surface of ngx-circular-slider
 */
export { NgxCircularSliderComponent } from "./lib/ngx-cs-slider/ngx-cs-slider.component";
export { NgxCircularSliderModule } from "./lib/ngx-circular-slider.module";

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jaXJjdWxhci1zbGlkZXIvIiwic291cmNlcyI6WyJwdWJsaWNfYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFJQSwyQ0FBYyw2Q0FBNkMsQ0FBQztBQUM1RCx3Q0FBYyxrQ0FBa0MsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2Ygbmd4LWNpcmN1bGFyLXNsaWRlclxuICovXG5cbmV4cG9ydCAqIGZyb20gXCIuL2xpYi9uZ3gtY3Mtc2xpZGVyL25neC1jcy1zbGlkZXIuY29tcG9uZW50XCI7XG5leHBvcnQgKiBmcm9tIFwiLi9saWIvbmd4LWNpcmN1bGFyLXNsaWRlci5tb2R1bGVcIjtcbmV4cG9ydCAqIGZyb20gXCIuL2xpYi9pbnRlcmZhY2VzXCI7XG4iXX0=