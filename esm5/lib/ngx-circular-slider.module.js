/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgxCircularSliderComponent } from "./ngx-cs-slider/ngx-cs-slider.component";
import { NgXCSClockFaceComponent } from "./ngx-cs-clock-face/ngx-cs-clock-face.component";
var NgxCircularSliderModule = /** @class */ (function () {
    function NgxCircularSliderModule() {
    }
    NgxCircularSliderModule.decorators = [
        { type: NgModule, args: [{
                    imports: [CommonModule],
                    declarations: [NgxCircularSliderComponent, NgXCSClockFaceComponent],
                    exports: [NgxCircularSliderComponent]
                },] },
    ];
    return NgxCircularSliderModule;
}());
export { NgxCircularSliderModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNpcmN1bGFyLXNsaWRlci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY2lyY3VsYXItc2xpZGVyLyIsInNvdXJjZXMiOlsibGliL25neC1jaXJjdWxhci1zbGlkZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxpREFBaUQsQ0FBQzs7Ozs7Z0JBRXpGLFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3ZCLFlBQVksRUFBRSxDQUFDLDBCQUEwQixFQUFFLHVCQUF1QixDQUFDO29CQUNuRSxPQUFPLEVBQUUsQ0FBQywwQkFBMEIsQ0FBQztpQkFDdEM7O2tDQVREOztTQVVhLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcbmltcG9ydCB7IE5neENpcmN1bGFyU2xpZGVyQ29tcG9uZW50IH0gZnJvbSBcIi4vbmd4LWNzLXNsaWRlci9uZ3gtY3Mtc2xpZGVyLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTmdYQ1NDbG9ja0ZhY2VDb21wb25lbnQgfSBmcm9tIFwiLi9uZ3gtY3MtY2xvY2stZmFjZS9uZ3gtY3MtY2xvY2stZmFjZS5jb21wb25lbnRcIjtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV0sXG4gIGRlY2xhcmF0aW9uczogW05neENpcmN1bGFyU2xpZGVyQ29tcG9uZW50LCBOZ1hDU0Nsb2NrRmFjZUNvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgTmd4Q2lyY3VsYXJTbGlkZXJNb2R1bGUge31cbiJdfQ==