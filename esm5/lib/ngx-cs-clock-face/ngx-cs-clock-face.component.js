/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, Input } from "@angular/core";
var /** @type {?} */ DEFAULT_RANGE = 48;
var /** @type {?} */ DEFAULT_TIME_RANGE = 12;
/**
 * @record
 */
export function IClockLines() { }
function IClockLines_tsickle_Closure_declarations() {
    /** @type {?} */
    IClockLines.prototype.id;
    /** @type {?} */
    IClockLines.prototype.strokeWidth;
    /** @type {?} */
    IClockLines.prototype.x1;
    /** @type {?} */
    IClockLines.prototype.y1;
    /** @type {?} */
    IClockLines.prototype.x2;
    /** @type {?} */
    IClockLines.prototype.y2;
}
var NgXCSClockFaceComponent = /** @class */ (function () {
    function NgXCSClockFaceComponent() {
        this.clockLines = [];
        this.clockTexts = [];
    }
    /**
     * @return {?}
     */
    NgXCSClockFaceComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.faceRadius = this.radius - 5;
        this.textRadius = this.radius - 26;
        this.createClockLines();
        this.createClockTexts();
    };
    /**
     * @return {?}
     */
    NgXCSClockFaceComponent.prototype.createClockLines = /**
     * @return {?}
     */
    function () {
        for (var /** @type {?} */ i = 0; i < DEFAULT_RANGE; i++) {
            var /** @type {?} */ cos = Math.cos(((2 * Math.PI) / DEFAULT_RANGE) * i);
            var /** @type {?} */ sin = Math.sin(((2 * Math.PI) / DEFAULT_RANGE) * i);
            this.clockLines.push({
                id: i,
                strokeWidth: i % 4 === 0 ? 3 : 1,
                x1: cos * this.faceRadius,
                y1: sin * this.faceRadius,
                x2: cos * (this.faceRadius - 7),
                y2: sin * (this.faceRadius - 7)
            });
        }
    };
    /**
     * @return {?}
     */
    NgXCSClockFaceComponent.prototype.createClockTexts = /**
     * @return {?}
     */
    function () {
        for (var /** @type {?} */ i = 0; i < DEFAULT_TIME_RANGE; i++) {
            this.clockTexts.push({
                id: i,
                x: this.textRadius *
                    Math.cos(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6),
                y: this.textRadius *
                    Math.sin(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6)
            });
        }
    };
    NgXCSClockFaceComponent.decorators = [
        { type: Component, args: [{
                    selector: "[ngx-cs-clock-face]",
                    templateUrl: "./ngx-cs-clock-face.component.html",
                    styleUrls: ["./ngx-cs-clock-face.component.scss"]
                },] },
    ];
    /** @nocollapse */
    NgXCSClockFaceComponent.ctorParameters = function () { return []; };
    NgXCSClockFaceComponent.propDecorators = {
        radius: [{ type: Input }],
        stroke: [{ type: Input }]
    };
    return NgXCSClockFaceComponent;
}());
export { NgXCSClockFaceComponent };
function NgXCSClockFaceComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.radius;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.stroke;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.faceRadius;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.textRadius;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.clockLines;
    /** @type {?} */
    NgXCSClockFaceComponent.prototype.clockTexts;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNzLWNsb2NrLWZhY2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LWNpcmN1bGFyLXNsaWRlci8iLCJzb3VyY2VzIjpbImxpYi9uZ3gtY3MtY2xvY2stZmFjZS9uZ3gtY3MtY2xvY2stZmFjZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBRTVELHFCQUFNLGFBQWEsR0FBRyxFQUFFLENBQUM7QUFDekIscUJBQU0sa0JBQWtCLEdBQUcsRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXlCNUI7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztLQUN0Qjs7OztJQUVELDZDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUVuQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztLQUN6Qjs7OztJQUVPLGtEQUFnQjs7OztRQUN0QixHQUFHLENBQUMsQ0FBQyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUN2QyxxQkFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMxRCxxQkFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztnQkFDbkIsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLEVBQUUsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVU7Z0JBQ3pCLEVBQUUsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVU7Z0JBQ3pCLEVBQUUsRUFBRSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztnQkFDL0IsRUFBRSxFQUFFLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2FBQ2hDLENBQUMsQ0FBQztTQUNKOzs7OztJQUdLLGtEQUFnQjs7OztRQUN0QixHQUFHLENBQUMsQ0FBQyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxrQkFBa0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQzVDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUNuQixFQUFFLEVBQUUsQ0FBQztnQkFDTCxDQUFDLEVBQ0MsSUFBSSxDQUFDLFVBQVU7b0JBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ2hFLENBQUMsRUFDQyxJQUFJLENBQUMsVUFBVTtvQkFDZixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNqRSxDQUFDLENBQUM7U0FDSjs7O2dCQXJESixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsV0FBVyxFQUFFLG9DQUFvQztvQkFDakQsU0FBUyxFQUFFLENBQUMsb0NBQW9DLENBQUM7aUJBQ2xEOzs7Ozt5QkFFRSxLQUFLO3lCQUNMLEtBQUs7O2tDQXJCUjs7U0FtQmEsdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuY29uc3QgREVGQVVMVF9SQU5HRSA9IDQ4O1xuY29uc3QgREVGQVVMVF9USU1FX1JBTkdFID0gMTI7XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUNsb2NrTGluZXMge1xuICBpZDogbnVtYmVyO1xuICBzdHJva2VXaWR0aDogbnVtYmVyO1xuICB4MTogbnVtYmVyO1xuICB5MTogbnVtYmVyO1xuICB4MjogbnVtYmVyO1xuICB5MjogbnVtYmVyO1xufVxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiW25neC1jcy1jbG9jay1mYWNlXVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL25neC1jcy1jbG9jay1mYWNlLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9uZ3gtY3MtY2xvY2stZmFjZS5jb21wb25lbnQuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBOZ1hDU0Nsb2NrRmFjZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG4gIEBJbnB1dCgpIHJhZGl1czogbnVtYmVyO1xuICBASW5wdXQoKSBzdHJva2U6IG51bWJlcjtcblxuICBwdWJsaWMgZmFjZVJhZGl1czogbnVtYmVyO1xuICBwdWJsaWMgdGV4dFJhZGl1czogbnVtYmVyO1xuICBwdWJsaWMgY2xvY2tMaW5lczogSUNsb2NrTGluZXNbXTtcbiAgcHVibGljIGNsb2NrVGV4dHM6IGFueVtdO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY2xvY2tMaW5lcyA9IFtdO1xuICAgIHRoaXMuY2xvY2tUZXh0cyA9IFtdO1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoKSB7XG4gICAgdGhpcy5mYWNlUmFkaXVzID0gdGhpcy5yYWRpdXMgLSA1O1xuICAgIHRoaXMudGV4dFJhZGl1cyA9IHRoaXMucmFkaXVzIC0gMjY7XG5cbiAgICB0aGlzLmNyZWF0ZUNsb2NrTGluZXMoKTtcbiAgICB0aGlzLmNyZWF0ZUNsb2NrVGV4dHMoKTtcbiAgfVxuXG4gIHByaXZhdGUgY3JlYXRlQ2xvY2tMaW5lcygpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IERFRkFVTFRfUkFOR0U7IGkrKykge1xuICAgICAgY29uc3QgY29zID0gTWF0aC5jb3MoKCgyICogTWF0aC5QSSkgLyBERUZBVUxUX1JBTkdFKSAqIGkpO1xuICAgICAgY29uc3Qgc2luID0gTWF0aC5zaW4oKCgyICogTWF0aC5QSSkgLyBERUZBVUxUX1JBTkdFKSAqIGkpO1xuICAgICAgdGhpcy5jbG9ja0xpbmVzLnB1c2goe1xuICAgICAgICBpZDogaSxcbiAgICAgICAgc3Ryb2tlV2lkdGg6IGkgJSA0ID09PSAwID8gMyA6IDEsXG4gICAgICAgIHgxOiBjb3MgKiB0aGlzLmZhY2VSYWRpdXMsXG4gICAgICAgIHkxOiBzaW4gKiB0aGlzLmZhY2VSYWRpdXMsXG4gICAgICAgIHgyOiBjb3MgKiAodGhpcy5mYWNlUmFkaXVzIC0gNyksXG4gICAgICAgIHkyOiBzaW4gKiAodGhpcy5mYWNlUmFkaXVzIC0gNylcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgY3JlYXRlQ2xvY2tUZXh0cygpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IERFRkFVTFRfVElNRV9SQU5HRTsgaSsrKSB7XG4gICAgICB0aGlzLmNsb2NrVGV4dHMucHVzaCh7XG4gICAgICAgIGlkOiBpLFxuICAgICAgICB4OlxuICAgICAgICAgIHRoaXMudGV4dFJhZGl1cyAqXG4gICAgICAgICAgTWF0aC5jb3MoKCgyICogTWF0aC5QSSkgLyAxMikgKiBpIC0gTWF0aC5QSSAvIDIgKyBNYXRoLlBJIC8gNiksXG4gICAgICAgIHk6XG4gICAgICAgICAgdGhpcy50ZXh0UmFkaXVzICpcbiAgICAgICAgICBNYXRoLnNpbigoKDIgKiBNYXRoLlBJKSAvIDEyKSAqIGkgLSBNYXRoLlBJIC8gMiArIE1hdGguUEkgLyA2KVxuICAgICAgfSk7XG4gICAgfVxuICB9XG59XG4iXX0=