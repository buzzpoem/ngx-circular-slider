import { Component, EventEmitter, Input, Output, ViewChild, NgModule } from '@angular/core';
import { fromEvent, merge } from 'rxjs';
import { interpolateHcl } from 'd3-interpolate';
import { switchMap, takeUntil, throttleTime } from 'rxjs/operators';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const /** @type {?} */ THROTTLE_DEFAULT = 50;
const /** @type {?} */ DEFAULT_PROPS = {
    segments: 6,
    strokeWidth: 40,
    radius: 145,
    gradientColorFrom: "#ff9800",
    gradientColorTo: "#ffcf00",
    bgCircleColor: "#171717",
    showClockFace: true,
    clockFaceColor: "#9d9d9d"
};
class NgxCircularSliderComponent {
    constructor() {
        this.update = new EventEmitter();
        this.props = DEFAULT_PROPS;
        this.startAngle = 0;
        this.angleLength = 0;
    }
    /**
     * @param {?} evt
     * @return {?}
     */
    static extractMouseEventCoords(evt) {
        const /** @type {?} */ coords = evt instanceof MouseEvent
            ? {
                x: evt.clientX,
                y: evt.clientY
            }
            : {
                x: evt.changedTouches.item(0).clientX,
                y: evt.changedTouches.item(0).clientY
            };
        return coords;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.setCircleCenter();
        this.onUpdate();
        this.setObservables();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.props) {
            this.props = changes.props.firstChange
                ? Object.assign(DEFAULT_PROPS, changes.props.currentValue)
                : DEFAULT_PROPS;
        }
        this.onUpdate();
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.closeStreams();
    }
    /**
     * @return {?}
     */
    onUpdate() {
        this.calcStartAndStop();
        this.createSegments();
        this.update.emit({
            startAngle: this.startAngle,
            angleLength: this.angleLength
        });
    }
    /**
     * @return {?}
     */
    setObservables() {
        const /** @type {?} */ mouseMove$ = merge(fromEvent(document, "mousemove"), fromEvent(document, "touchmove"));
        const /** @type {?} */ mouseUp$ = merge(fromEvent(document, "mouseup"), fromEvent(document, "touchend"));
        //     this.startSubscription = merge(
        //       fromEvent(this.startIcon.nativeElement, "touchstart"),
        //       fromEvent(this.startIcon.nativeElement, "mousedown")
        //     )
        //       .pipe(
        //         switchMap(_ =>
        //           mouseMove$.pipe(
        //             takeUntil(mouseUp$),
        //             throttleTime(THROTTLE_DEFAULT)
        //           )
        //         )
        //       )
        //       .subscribe((res: MouseEvent | TouchEvent) => {
        //         this.handleStartPan(res);
        //       });
        this.stopSubscription = merge(fromEvent(this.stopIcon.nativeElement, "touchstart"), fromEvent(this.stopIcon.nativeElement, "mousedown"))
            .pipe(switchMap(_ => mouseMove$.pipe(takeUntil(mouseUp$), throttleTime(THROTTLE_DEFAULT))))
            .subscribe((res) => {
            this.handleStopPan(res);
        });
    }
    /**
     * @return {?}
     */
    closeStreams() {
        //     if (this.startSubscription) {
        //       this.startSubscription.unsubscribe();
        //       this.startSubscription = null;
        //     }
        if (this.stopSubscription) {
            this.stopSubscription.unsubscribe();
            this.stopSubscription = null;
        }
    }
    /**
     * @param {?} evt
     * @return {?}
     */
    handleStartPan(evt) {
        const /** @type {?} */ coords = NgxCircularSliderComponent.extractMouseEventCoords(evt);
        this.setCircleCenter();
        const /** @type {?} */ currentAngleStop = (this.startAngle + this.angleLength) % (2 * Math.PI);
        let /** @type {?} */ newAngle = Math.atan2(coords.y - this.circleCenterY, coords.x - this.circleCenterX) +
            Math.PI / 2;
        if (newAngle < 0) {
            newAngle += 2 * Math.PI;
        }
        let /** @type {?} */ newAngleLength = currentAngleStop - newAngle;
        if (newAngleLength < 0) {
            newAngleLength += 2 * Math.PI;
        }
        this.startAngle = newAngle;
        this.angleLength = newAngleLength % (2 * Math.PI);
        this.onUpdate();
    }
    /**
     * @param {?} evt
     * @return {?}
     */
    handleStopPan(evt) {
        const /** @type {?} */ coords = NgxCircularSliderComponent.extractMouseEventCoords(evt);
        this.setCircleCenter();
        const /** @type {?} */ newAngle = Math.atan2(coords.y - this.circleCenterY, coords.x - this.circleCenterX) +
            Math.PI / 2;
        let /** @type {?} */ newAngleLength = (newAngle - this.startAngle) % (2 * Math.PI);
        if (newAngleLength < 0) {
            newAngleLength += 2 * Math.PI;
        }
        this.angleLength = newAngleLength;
        this.onUpdate();
    }
    /**
     * @return {?}
     */
    calcStartAndStop() {
        this.start = this.calculateArcCircle(0, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
        this.stop = this.calculateArcCircle(this.props.segments - 1, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
    }
    /**
     * @param {?} index
     * @param {?} segments
     * @param {?} gradientColorFrom
     * @param {?} gradientColorTo
     * @return {?}
     */
    calculateArcColor(index, segments, gradientColorFrom, gradientColorTo) {
        const /** @type {?} */ interpolate = interpolateHcl(gradientColorFrom, gradientColorTo);
        return {
            fromColor: interpolate(index / segments),
            toColor: interpolate((index + 1) / segments)
        };
    }
    /**
     * @param {?} indexInput
     * @param {?} segments
     * @param {?} radius
     * @param {?=} startAngleInput
     * @param {?=} angleLengthInput
     * @return {?}
     */
    calculateArcCircle(indexInput, segments, radius, startAngleInput = 0, angleLengthInput = 2 * Math.PI) {
        // Add 0.0001 to the possible angle so when start = stop angle, whole circle is drawn
        const /** @type {?} */ startAngle = startAngleInput % (2 * Math.PI);
        const /** @type {?} */ angleLength = angleLengthInput % (2 * Math.PI);
        const /** @type {?} */ index = indexInput + 1;
        const /** @type {?} */ fromAngle = (angleLength / segments) * (index - 1) + startAngle;
        const /** @type {?} */ toAngle = (angleLength / segments) * index + startAngle;
        const /** @type {?} */ fromX = radius * Math.sin(fromAngle);
        const /** @type {?} */ fromY = -radius * Math.cos(fromAngle);
        const /** @type {?} */ realToX = radius * Math.sin(toAngle);
        const /** @type {?} */ realToY = -radius * Math.cos(toAngle);
        // add 0.005 to start drawing a little bit earlier so segments stick together
        const /** @type {?} */ toX = radius * Math.sin(toAngle + 0.005);
        const /** @type {?} */ toY = -radius * Math.cos(toAngle + 0.005);
        return {
            fromX,
            fromY,
            toX,
            toY,
            realToX,
            realToY
        };
    }
    /**
     * @return {?}
     */
    createSegments() {
        this.segments = [];
        for (let /** @type {?} */ i = 0; i < this.props.segments; i++) {
            const /** @type {?} */ id = i;
            const /** @type {?} */ colors = this.calculateArcColor(id, this.props.segments, this.props.gradientColorFrom, this.props.gradientColorTo);
            const /** @type {?} */ arcs = this.calculateArcCircle(id, this.props.segments, this.props.radius, this.startAngle, this.angleLength);
            this.segments.push({
                id: id,
                d: `M ${arcs.fromX.toFixed(2)} ${arcs.fromY.toFixed(2)} A ${this.props.radius} ${this.props.radius} 
        0 0 1 ${arcs.toX.toFixed(2)} ${arcs.toY.toFixed(2)}`,
                colors: Object.assign({}, colors),
                arcs: Object.assign({}, arcs)
            });
        }
    }
    /**
     * @return {?}
     */
    setCircleCenter() {
        // todo: nicer solution to use document.body?
        const /** @type {?} */ bodyRect = document.body.getBoundingClientRect();
        const /** @type {?} */ elemRect = this.circle.nativeElement.getBoundingClientRect();
        const /** @type {?} */ px = elemRect.left - bodyRect.left;
        const /** @type {?} */ py = elemRect.top - bodyRect.top;
        const /** @type {?} */ halfOfContainer = this.getContainerWidth() / 2;
        this.circleCenterX = px + halfOfContainer;
        this.circleCenterY = py + halfOfContainer;
    }
    /**
     * @return {?}
     */
    getContainerWidth() {
        const { strokeWidth, radius } = this.props;
        return strokeWidth + radius * 2 + 2;
    }
    /**
     * @param {?} index
     * @return {?}
     */
    getGradientId(index) {
        return `gradient${index}`;
    }
    /**
     * @param {?} index
     * @return {?}
     */
    getGradientUrl(index) {
        return 'url(' + window.location + '#gradient' + index + ')';
    }
    /**
     * @return {?}
     */
    getTranslate() {
        return ` translate(
  ${this.props.strokeWidth / 2 + this.props.radius + 1},
  ${this.props.strokeWidth / 2 + this.props.radius + 1} )`;
    }
    /**
     * @param {?} x
     * @param {?} y
     * @return {?}
     */
    getTranslateFrom(x, y) {
        return ` translate(${x}, ${y})`;
    }
}
NgxCircularSliderComponent.decorators = [
    { type: Component, args: [{
                selector: "ngx-cs-slider",
                templateUrl: "./ngx-cs-slider.component.html",
                styleUrls: ["./ngx-cs-slider.component.scss"]
            },] },
];
/** @nocollapse */
NgxCircularSliderComponent.ctorParameters = () => [];
NgxCircularSliderComponent.propDecorators = {
    props: [{ type: Input }],
    startAngle: [{ type: Input }],
    angleLength: [{ type: Input }],
    update: [{ type: Output }],
    circle: [{ type: ViewChild, args: ["circle",] }],
    stopIcon: [{ type: ViewChild, args: ["stopIcon",] }],
    startIcon: [{ type: ViewChild, args: ["startIcon",] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
const /** @type {?} */ DEFAULT_RANGE = 48;
const /** @type {?} */ DEFAULT_TIME_RANGE = 12;
class NgXCSClockFaceComponent {
    constructor() {
        this.clockLines = [];
        this.clockTexts = [];
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.faceRadius = this.radius - 5;
        this.textRadius = this.radius - 26;
        this.createClockLines();
        this.createClockTexts();
    }
    /**
     * @return {?}
     */
    createClockLines() {
        for (let /** @type {?} */ i = 0; i < DEFAULT_RANGE; i++) {
            const /** @type {?} */ cos = Math.cos(((2 * Math.PI) / DEFAULT_RANGE) * i);
            const /** @type {?} */ sin = Math.sin(((2 * Math.PI) / DEFAULT_RANGE) * i);
            this.clockLines.push({
                id: i,
                strokeWidth: i % 4 === 0 ? 3 : 1,
                x1: cos * this.faceRadius,
                y1: sin * this.faceRadius,
                x2: cos * (this.faceRadius - 7),
                y2: sin * (this.faceRadius - 7)
            });
        }
    }
    /**
     * @return {?}
     */
    createClockTexts() {
        for (let /** @type {?} */ i = 0; i < DEFAULT_TIME_RANGE; i++) {
            this.clockTexts.push({
                id: i,
                x: this.textRadius *
                    Math.cos(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6),
                y: this.textRadius *
                    Math.sin(((2 * Math.PI) / 12) * i - Math.PI / 2 + Math.PI / 6)
            });
        }
    }
}
NgXCSClockFaceComponent.decorators = [
    { type: Component, args: [{
                selector: "[ngx-cs-clock-face]",
                templateUrl: "./ngx-cs-clock-face.component.html",
                styleUrls: ["./ngx-cs-clock-face.component.scss"]
            },] },
];
/** @nocollapse */
NgXCSClockFaceComponent.ctorParameters = () => [];
NgXCSClockFaceComponent.propDecorators = {
    radius: [{ type: Input }],
    stroke: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxCircularSliderModule {
}
NgxCircularSliderModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                declarations: [NgxCircularSliderComponent, NgXCSClockFaceComponent],
                exports: [NgxCircularSliderComponent]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { NgxCircularSliderComponent, NgxCircularSliderModule, NgXCSClockFaceComponent as ɵa };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNpcmN1bGFyLXNsaWRlci5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vbmd4LWNpcmN1bGFyLXNsaWRlci9saWIvbmd4LWNzLXNsaWRlci9uZ3gtY3Mtc2xpZGVyLmNvbXBvbmVudC50cyIsIm5nOi8vbmd4LWNpcmN1bGFyLXNsaWRlci9saWIvbmd4LWNzLWNsb2NrLWZhY2Uvbmd4LWNzLWNsb2NrLWZhY2UuY29tcG9uZW50LnRzIiwibmc6Ly9uZ3gtY2lyY3VsYXItc2xpZGVyL2xpYi9uZ3gtY2lyY3VsYXItc2xpZGVyLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBFbGVtZW50UmVmLFxyXG4gIEV2ZW50RW1pdHRlcixcclxuICBJbnB1dCxcclxuICBPbkNoYW5nZXMsXHJcbiAgT25EZXN0cm95LFxyXG4gIE9uSW5pdCxcclxuICBPdXRwdXQsXHJcbiAgVmlld0NoaWxkXHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgZnJvbUV2ZW50LCBtZXJnZSwgU3Vic2NyaXB0aW9uIH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgaW50ZXJwb2xhdGVIY2wgfSBmcm9tIFwiZDMtaW50ZXJwb2xhdGVcIjtcclxuaW1wb3J0IHtcclxuICBJQXJjLFxyXG4gIElDb2xvcixcclxuICBJQ29vcmRzLFxyXG4gIElPdXRwdXQsXHJcbiAgSVByb3BzLFxyXG4gIElTZWdtZW50LFxyXG4gIElTbGlkZXJDaGFuZ2VzXHJcbn0gZnJvbSBcIi4uL2ludGVyZmFjZXNcIjtcclxuaW1wb3J0IHsgc3dpdGNoTWFwLCB0YWtlVW50aWwsIHRocm90dGxlVGltZSB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5cclxuY29uc3QgVEhST1RUTEVfREVGQVVMVCA9IDUwO1xyXG5jb25zdCBERUZBVUxUX1BST1BTOiBJUHJvcHMgPSB7XHJcbiAgc2VnbWVudHM6IDYsXHJcbiAgc3Ryb2tlV2lkdGg6IDQwLFxyXG4gIHJhZGl1czogMTQ1LFxyXG4gIGdyYWRpZW50Q29sb3JGcm9tOiBcIiNmZjk4MDBcIixcclxuICBncmFkaWVudENvbG9yVG86IFwiI2ZmY2YwMFwiLFxyXG4gIGJnQ2lyY2xlQ29sb3I6IFwiIzE3MTcxN1wiLFxyXG4gIHNob3dDbG9ja0ZhY2U6IHRydWUsXHJcbiAgY2xvY2tGYWNlQ29sb3I6IFwiIzlkOWQ5ZFwiXHJcbn07XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJuZ3gtY3Mtc2xpZGVyXCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9uZ3gtY3Mtc2xpZGVyLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL25neC1jcy1zbGlkZXIuY29tcG9uZW50LnNjc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5neENpcmN1bGFyU2xpZGVyQ29tcG9uZW50XHJcbiAgaW1wbGVtZW50cyBPbkNoYW5nZXMsIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICBASW5wdXQoKSBwcm9wczogSVByb3BzO1xyXG4gIEBJbnB1dCgpIHN0YXJ0QW5nbGU6IG51bWJlcjtcclxuICBASW5wdXQoKSBhbmdsZUxlbmd0aDogbnVtYmVyO1xyXG4gIEBPdXRwdXQoKSB1cGRhdGU6IEV2ZW50RW1pdHRlcjxJT3V0cHV0PiA9IG5ldyBFdmVudEVtaXR0ZXI8SU91dHB1dD4oKTtcclxuICBwdWJsaWMgc2VnbWVudHM6IElTZWdtZW50W107XHJcbiAgcHVibGljIHN0YXJ0OiBJQXJjO1xyXG4gIHB1YmxpYyBzdG9wOiBJQXJjO1xyXG4vLyAgIHByaXZhdGUgc3RhcnRTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuICBwcml2YXRlIHN0b3BTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuICBwcml2YXRlIGNpcmNsZUNlbnRlclg6IG51bWJlcjtcclxuICBwcml2YXRlIGNpcmNsZUNlbnRlclk6IG51bWJlcjtcclxuICBAVmlld0NoaWxkKFwiY2lyY2xlXCIpIHByaXZhdGUgY2lyY2xlOiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJzdG9wSWNvblwiKSBwcml2YXRlIHN0b3BJY29uOiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJzdGFydEljb25cIikgcHJpdmF0ZSBzdGFydEljb246IEVsZW1lbnRSZWY7XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIGV4dHJhY3RNb3VzZUV2ZW50Q29vcmRzKGV2dDogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQpIHtcclxuICAgIGNvbnN0IGNvb3JkczogSUNvb3JkcyA9XHJcbiAgICAgIGV2dCBpbnN0YW5jZW9mIE1vdXNlRXZlbnRcclxuICAgICAgICA/IHtcclxuICAgICAgICAgICAgeDogZXZ0LmNsaWVudFgsXHJcbiAgICAgICAgICAgIHk6IGV2dC5jbGllbnRZXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgOiB7XHJcbiAgICAgICAgICAgIHg6IGV2dC5jaGFuZ2VkVG91Y2hlcy5pdGVtKDApLmNsaWVudFgsXHJcbiAgICAgICAgICAgIHk6IGV2dC5jaGFuZ2VkVG91Y2hlcy5pdGVtKDApLmNsaWVudFlcclxuICAgICAgICAgIH07XHJcbiAgICByZXR1cm4gY29vcmRzO1xyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB0aGlzLnByb3BzID0gREVGQVVMVF9QUk9QUztcclxuICAgIHRoaXMuc3RhcnRBbmdsZSA9IDA7XHJcbiAgICB0aGlzLmFuZ2xlTGVuZ3RoID0gMDtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5zZXRDaXJjbGVDZW50ZXIoKTtcclxuICAgIHRoaXMub25VcGRhdGUoKTtcclxuICAgIHRoaXMuc2V0T2JzZXJ2YWJsZXMoKTtcclxuICB9XHJcblxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IElTbGlkZXJDaGFuZ2VzKSB7XHJcbiAgICBpZiAoY2hhbmdlcy5wcm9wcykge1xyXG4gICAgICB0aGlzLnByb3BzID0gY2hhbmdlcy5wcm9wcy5maXJzdENoYW5nZVxyXG4gICAgICAgID8gT2JqZWN0LmFzc2lnbihERUZBVUxUX1BST1BTLCBjaGFuZ2VzLnByb3BzLmN1cnJlbnRWYWx1ZSlcclxuICAgICAgICA6IERFRkFVTFRfUFJPUFM7XHJcbiAgICB9XHJcbiAgICB0aGlzLm9uVXBkYXRlKCk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMuY2xvc2VTdHJlYW1zKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIG9uVXBkYXRlKCkge1xyXG4gICAgdGhpcy5jYWxjU3RhcnRBbmRTdG9wKCk7XHJcbiAgICB0aGlzLmNyZWF0ZVNlZ21lbnRzKCk7XHJcbiAgICB0aGlzLnVwZGF0ZS5lbWl0KHtcclxuICAgICAgc3RhcnRBbmdsZTogdGhpcy5zdGFydEFuZ2xlLFxyXG4gICAgICBhbmdsZUxlbmd0aDogdGhpcy5hbmdsZUxlbmd0aFxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNldE9ic2VydmFibGVzKCkge1xyXG4gICAgY29uc3QgbW91c2VNb3ZlJCA9IG1lcmdlKFxyXG4gICAgICBmcm9tRXZlbnQoZG9jdW1lbnQsIFwibW91c2Vtb3ZlXCIpLFxyXG4gICAgICBmcm9tRXZlbnQoZG9jdW1lbnQsIFwidG91Y2htb3ZlXCIpXHJcbiAgICApO1xyXG4gICAgY29uc3QgbW91c2VVcCQgPSBtZXJnZShcclxuICAgICAgZnJvbUV2ZW50KGRvY3VtZW50LCBcIm1vdXNldXBcIiksXHJcbiAgICAgIGZyb21FdmVudChkb2N1bWVudCwgXCJ0b3VjaGVuZFwiKVxyXG4gICAgKTtcclxuXHJcbi8vICAgICB0aGlzLnN0YXJ0U3Vic2NyaXB0aW9uID0gbWVyZ2UoXHJcbi8vICAgICAgIGZyb21FdmVudCh0aGlzLnN0YXJ0SWNvbi5uYXRpdmVFbGVtZW50LCBcInRvdWNoc3RhcnRcIiksXHJcbi8vICAgICAgIGZyb21FdmVudCh0aGlzLnN0YXJ0SWNvbi5uYXRpdmVFbGVtZW50LCBcIm1vdXNlZG93blwiKVxyXG4vLyAgICAgKVxyXG4vLyAgICAgICAucGlwZShcclxuLy8gICAgICAgICBzd2l0Y2hNYXAoXyA9PlxyXG4vLyAgICAgICAgICAgbW91c2VNb3ZlJC5waXBlKFxyXG4vLyAgICAgICAgICAgICB0YWtlVW50aWwobW91c2VVcCQpLFxyXG4vLyAgICAgICAgICAgICB0aHJvdHRsZVRpbWUoVEhST1RUTEVfREVGQVVMVClcclxuLy8gICAgICAgICAgIClcclxuLy8gICAgICAgICApXHJcbi8vICAgICAgIClcclxuLy8gICAgICAgLnN1YnNjcmliZSgocmVzOiBNb3VzZUV2ZW50IHwgVG91Y2hFdmVudCkgPT4ge1xyXG4vLyAgICAgICAgIHRoaXMuaGFuZGxlU3RhcnRQYW4ocmVzKTtcclxuLy8gICAgICAgfSk7XHJcblxyXG4gICAgdGhpcy5zdG9wU3Vic2NyaXB0aW9uID0gbWVyZ2UoXHJcbiAgICAgIGZyb21FdmVudCh0aGlzLnN0b3BJY29uLm5hdGl2ZUVsZW1lbnQsIFwidG91Y2hzdGFydFwiKSxcclxuICAgICAgZnJvbUV2ZW50KHRoaXMuc3RvcEljb24ubmF0aXZlRWxlbWVudCwgXCJtb3VzZWRvd25cIilcclxuICAgIClcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgc3dpdGNoTWFwKF8gPT5cclxuICAgICAgICAgIG1vdXNlTW92ZSQucGlwZShcclxuICAgICAgICAgICAgdGFrZVVudGlsKG1vdXNlVXAkKSxcclxuICAgICAgICAgICAgdGhyb3R0bGVUaW1lKFRIUk9UVExFX0RFRkFVTFQpXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoKHJlczogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQpID0+IHtcclxuICAgICAgICB0aGlzLmhhbmRsZVN0b3BQYW4ocmVzKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNsb3NlU3RyZWFtcygpIHtcclxuLy8gICAgIGlmICh0aGlzLnN0YXJ0U3Vic2NyaXB0aW9uKSB7XHJcbi8vICAgICAgIHRoaXMuc3RhcnRTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuLy8gICAgICAgdGhpcy5zdGFydFN1YnNjcmlwdGlvbiA9IG51bGw7XHJcbi8vICAgICB9XHJcbiAgICBpZiAodGhpcy5zdG9wU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgIHRoaXMuc3RvcFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgICB0aGlzLnN0b3BTdWJzY3JpcHRpb24gPSBudWxsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBoYW5kbGVTdGFydFBhbihldnQ6IE1vdXNlRXZlbnQgfCBUb3VjaEV2ZW50KSB7XHJcbiAgICBjb25zdCBjb29yZHMgPSBOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudC5leHRyYWN0TW91c2VFdmVudENvb3JkcyhldnQpO1xyXG5cclxuICAgIHRoaXMuc2V0Q2lyY2xlQ2VudGVyKCk7XHJcbiAgICBjb25zdCBjdXJyZW50QW5nbGVTdG9wID1cclxuICAgICAgKHRoaXMuc3RhcnRBbmdsZSArIHRoaXMuYW5nbGVMZW5ndGgpICUgKDIgKiBNYXRoLlBJKTtcclxuICAgIGxldCBuZXdBbmdsZSA9XHJcbiAgICAgIE1hdGguYXRhbjIoY29vcmRzLnkgLSB0aGlzLmNpcmNsZUNlbnRlclksIGNvb3Jkcy54IC0gdGhpcy5jaXJjbGVDZW50ZXJYKSArXHJcbiAgICAgIE1hdGguUEkgLyAyO1xyXG5cclxuICAgIGlmIChuZXdBbmdsZSA8IDApIHtcclxuICAgICAgbmV3QW5nbGUgKz0gMiAqIE1hdGguUEk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IG5ld0FuZ2xlTGVuZ3RoID0gY3VycmVudEFuZ2xlU3RvcCAtIG5ld0FuZ2xlO1xyXG4gICAgaWYgKG5ld0FuZ2xlTGVuZ3RoIDwgMCkge1xyXG4gICAgICBuZXdBbmdsZUxlbmd0aCArPSAyICogTWF0aC5QSTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnN0YXJ0QW5nbGUgPSBuZXdBbmdsZTtcclxuICAgIHRoaXMuYW5nbGVMZW5ndGggPSBuZXdBbmdsZUxlbmd0aCAlICgyICogTWF0aC5QSSk7XHJcblxyXG4gICAgdGhpcy5vblVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBoYW5kbGVTdG9wUGFuKGV2dDogTW91c2VFdmVudCB8IFRvdWNoRXZlbnQpIHtcclxuICAgIGNvbnN0IGNvb3JkcyA9IE5neENpcmN1bGFyU2xpZGVyQ29tcG9uZW50LmV4dHJhY3RNb3VzZUV2ZW50Q29vcmRzKGV2dCk7XHJcbiAgICB0aGlzLnNldENpcmNsZUNlbnRlcigpO1xyXG4gICAgY29uc3QgbmV3QW5nbGUgPVxyXG4gICAgICBNYXRoLmF0YW4yKGNvb3Jkcy55IC0gdGhpcy5jaXJjbGVDZW50ZXJZLCBjb29yZHMueCAtIHRoaXMuY2lyY2xlQ2VudGVyWCkgK1xyXG4gICAgICBNYXRoLlBJIC8gMjtcclxuICAgIGxldCBuZXdBbmdsZUxlbmd0aCA9IChuZXdBbmdsZSAtIHRoaXMuc3RhcnRBbmdsZSkgJSAoMiAqIE1hdGguUEkpO1xyXG5cclxuICAgIGlmIChuZXdBbmdsZUxlbmd0aCA8IDApIHtcclxuICAgICAgbmV3QW5nbGVMZW5ndGggKz0gMiAqIE1hdGguUEk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5hbmdsZUxlbmd0aCA9IG5ld0FuZ2xlTGVuZ3RoO1xyXG4gICAgdGhpcy5vblVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjYWxjU3RhcnRBbmRTdG9wKCkge1xyXG4gICAgdGhpcy5zdGFydCA9IHRoaXMuY2FsY3VsYXRlQXJjQ2lyY2xlKFxyXG4gICAgICAwLFxyXG4gICAgICB0aGlzLnByb3BzLnNlZ21lbnRzLFxyXG4gICAgICB0aGlzLnByb3BzLnJhZGl1cyxcclxuICAgICAgdGhpcy5zdGFydEFuZ2xlLFxyXG4gICAgICB0aGlzLmFuZ2xlTGVuZ3RoXHJcbiAgICApO1xyXG4gICAgdGhpcy5zdG9wID0gdGhpcy5jYWxjdWxhdGVBcmNDaXJjbGUoXHJcbiAgICAgIHRoaXMucHJvcHMuc2VnbWVudHMgLSAxLFxyXG4gICAgICB0aGlzLnByb3BzLnNlZ21lbnRzLFxyXG4gICAgICB0aGlzLnByb3BzLnJhZGl1cyxcclxuICAgICAgdGhpcy5zdGFydEFuZ2xlLFxyXG4gICAgICB0aGlzLmFuZ2xlTGVuZ3RoXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjYWxjdWxhdGVBcmNDb2xvcihcclxuICAgIGluZGV4LFxyXG4gICAgc2VnbWVudHMsXHJcbiAgICBncmFkaWVudENvbG9yRnJvbSxcclxuICAgIGdyYWRpZW50Q29sb3JUb1xyXG4gICkge1xyXG4gICAgY29uc3QgaW50ZXJwb2xhdGUgPSBpbnRlcnBvbGF0ZUhjbChncmFkaWVudENvbG9yRnJvbSwgZ3JhZGllbnRDb2xvclRvKTtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBmcm9tQ29sb3I6IGludGVycG9sYXRlKGluZGV4IC8gc2VnbWVudHMpLFxyXG4gICAgICB0b0NvbG9yOiBpbnRlcnBvbGF0ZSgoaW5kZXggKyAxKSAvIHNlZ21lbnRzKVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY2FsY3VsYXRlQXJjQ2lyY2xlKFxyXG4gICAgaW5kZXhJbnB1dCxcclxuICAgIHNlZ21lbnRzLFxyXG4gICAgcmFkaXVzLFxyXG4gICAgc3RhcnRBbmdsZUlucHV0ID0gMCxcclxuICAgIGFuZ2xlTGVuZ3RoSW5wdXQgPSAyICogTWF0aC5QSVxyXG4gICkge1xyXG4gICAgLy8gQWRkIDAuMDAwMSB0byB0aGUgcG9zc2libGUgYW5nbGUgc28gd2hlbiBzdGFydCA9IHN0b3AgYW5nbGUsIHdob2xlIGNpcmNsZSBpcyBkcmF3blxyXG4gICAgY29uc3Qgc3RhcnRBbmdsZSA9IHN0YXJ0QW5nbGVJbnB1dCAlICgyICogTWF0aC5QSSk7XHJcbiAgICBjb25zdCBhbmdsZUxlbmd0aCA9IGFuZ2xlTGVuZ3RoSW5wdXQgJSAoMiAqIE1hdGguUEkpO1xyXG4gICAgY29uc3QgaW5kZXggPSBpbmRleElucHV0ICsgMTtcclxuICAgIGNvbnN0IGZyb21BbmdsZSA9IChhbmdsZUxlbmd0aCAvIHNlZ21lbnRzKSAqIChpbmRleCAtIDEpICsgc3RhcnRBbmdsZTtcclxuICAgIGNvbnN0IHRvQW5nbGUgPSAoYW5nbGVMZW5ndGggLyBzZWdtZW50cykgKiBpbmRleCArIHN0YXJ0QW5nbGU7XHJcbiAgICBjb25zdCBmcm9tWCA9IHJhZGl1cyAqIE1hdGguc2luKGZyb21BbmdsZSk7XHJcbiAgICBjb25zdCBmcm9tWSA9IC1yYWRpdXMgKiBNYXRoLmNvcyhmcm9tQW5nbGUpO1xyXG4gICAgY29uc3QgcmVhbFRvWCA9IHJhZGl1cyAqIE1hdGguc2luKHRvQW5nbGUpO1xyXG4gICAgY29uc3QgcmVhbFRvWSA9IC1yYWRpdXMgKiBNYXRoLmNvcyh0b0FuZ2xlKTtcclxuXHJcbiAgICAvLyBhZGQgMC4wMDUgdG8gc3RhcnQgZHJhd2luZyBhIGxpdHRsZSBiaXQgZWFybGllciBzbyBzZWdtZW50cyBzdGljayB0b2dldGhlclxyXG4gICAgY29uc3QgdG9YID0gcmFkaXVzICogTWF0aC5zaW4odG9BbmdsZSArIDAuMDA1KTtcclxuICAgIGNvbnN0IHRvWSA9IC1yYWRpdXMgKiBNYXRoLmNvcyh0b0FuZ2xlICsgMC4wMDUpO1xyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgIGZyb21YLFxyXG4gICAgICBmcm9tWSxcclxuICAgICAgdG9YLFxyXG4gICAgICB0b1ksXHJcbiAgICAgIHJlYWxUb1gsXHJcbiAgICAgIHJlYWxUb1lcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNyZWF0ZVNlZ21lbnRzKCkge1xyXG4gICAgdGhpcy5zZWdtZW50cyA9IFtdO1xyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnByb3BzLnNlZ21lbnRzOyBpKyspIHtcclxuICAgICAgY29uc3QgaWQgPSBpO1xyXG4gICAgICBjb25zdCBjb2xvcnM6IElDb2xvciA9IHRoaXMuY2FsY3VsYXRlQXJjQ29sb3IoXHJcbiAgICAgICAgaWQsXHJcbiAgICAgICAgdGhpcy5wcm9wcy5zZWdtZW50cyxcclxuICAgICAgICB0aGlzLnByb3BzLmdyYWRpZW50Q29sb3JGcm9tLFxyXG4gICAgICAgIHRoaXMucHJvcHMuZ3JhZGllbnRDb2xvclRvXHJcbiAgICAgICk7XHJcbiAgICAgIGNvbnN0IGFyY3M6IElBcmMgPSB0aGlzLmNhbGN1bGF0ZUFyY0NpcmNsZShcclxuICAgICAgICBpZCxcclxuICAgICAgICB0aGlzLnByb3BzLnNlZ21lbnRzLFxyXG4gICAgICAgIHRoaXMucHJvcHMucmFkaXVzLFxyXG4gICAgICAgIHRoaXMuc3RhcnRBbmdsZSxcclxuICAgICAgICB0aGlzLmFuZ2xlTGVuZ3RoXHJcbiAgICAgICk7XHJcblxyXG4gICAgICB0aGlzLnNlZ21lbnRzLnB1c2goe1xyXG4gICAgICAgIGlkOiBpZCxcclxuICAgICAgICBkOiBgTSAke2FyY3MuZnJvbVgudG9GaXhlZCgyKX0gJHthcmNzLmZyb21ZLnRvRml4ZWQoMil9IEEgJHtcclxuICAgICAgICAgIHRoaXMucHJvcHMucmFkaXVzXHJcbiAgICAgICAgfSAke3RoaXMucHJvcHMucmFkaXVzfSBcclxuICAgICAgICAwIDAgMSAke2FyY3MudG9YLnRvRml4ZWQoMil9ICR7YXJjcy50b1kudG9GaXhlZCgyKX1gLFxyXG4gICAgICAgIGNvbG9yczogT2JqZWN0LmFzc2lnbih7fSwgY29sb3JzKSxcclxuICAgICAgICBhcmNzOiBPYmplY3QuYXNzaWduKHt9LCBhcmNzKVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgc2V0Q2lyY2xlQ2VudGVyKCkge1xyXG4gICAgLy8gdG9kbzogbmljZXIgc29sdXRpb24gdG8gdXNlIGRvY3VtZW50LmJvZHk/XHJcbiAgICBjb25zdCBib2R5UmVjdCA9IGRvY3VtZW50LmJvZHkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICBjb25zdCBlbGVtUmVjdCA9IHRoaXMuY2lyY2xlLm5hdGl2ZUVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICBjb25zdCBweCA9IGVsZW1SZWN0LmxlZnQgLSBib2R5UmVjdC5sZWZ0O1xyXG4gICAgY29uc3QgcHkgPSBlbGVtUmVjdC50b3AgLSBib2R5UmVjdC50b3A7XHJcbiAgICBjb25zdCBoYWxmT2ZDb250YWluZXIgPSB0aGlzLmdldENvbnRhaW5lcldpZHRoKCkgLyAyO1xyXG4gICAgdGhpcy5jaXJjbGVDZW50ZXJYID0gcHggKyBoYWxmT2ZDb250YWluZXI7XHJcbiAgICB0aGlzLmNpcmNsZUNlbnRlclkgPSBweSArIGhhbGZPZkNvbnRhaW5lcjtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRDb250YWluZXJXaWR0aCgpIHtcclxuICAgIGNvbnN0IHsgc3Ryb2tlV2lkdGgsIHJhZGl1cyB9ID0gdGhpcy5wcm9wcztcclxuICAgIHJldHVybiBzdHJva2VXaWR0aCArIHJhZGl1cyAqIDIgKyAyO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldEdyYWRpZW50SWQoaW5kZXgpIHtcclxuICAgIHJldHVybiBgZ3JhZGllbnQke2luZGV4fWA7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0R3JhZGllbnRVcmwoaW5kZXgpIHtcclxuICAgIHJldHVybiAndXJsKCcrd2luZG93LmxvY2F0aW9uKycjZ3JhZGllbnQnK2luZGV4KycpJztcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRUcmFuc2xhdGUoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiBgIHRyYW5zbGF0ZShcclxuICAke3RoaXMucHJvcHMuc3Ryb2tlV2lkdGggLyAyICsgdGhpcy5wcm9wcy5yYWRpdXMgKyAxfSxcclxuICAke3RoaXMucHJvcHMuc3Ryb2tlV2lkdGggLyAyICsgdGhpcy5wcm9wcy5yYWRpdXMgKyAxfSApYDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRUcmFuc2xhdGVGcm9tKHgsIHkpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIGAgdHJhbnNsYXRlKCR7eH0sICR7eX0pYDtcclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuY29uc3QgREVGQVVMVF9SQU5HRSA9IDQ4O1xuY29uc3QgREVGQVVMVF9USU1FX1JBTkdFID0gMTI7XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUNsb2NrTGluZXMge1xuICBpZDogbnVtYmVyO1xuICBzdHJva2VXaWR0aDogbnVtYmVyO1xuICB4MTogbnVtYmVyO1xuICB5MTogbnVtYmVyO1xuICB4MjogbnVtYmVyO1xuICB5MjogbnVtYmVyO1xufVxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwiW25neC1jcy1jbG9jay1mYWNlXVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL25neC1jcy1jbG9jay1mYWNlLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9uZ3gtY3MtY2xvY2stZmFjZS5jb21wb25lbnQuc2Nzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBOZ1hDU0Nsb2NrRmFjZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG4gIEBJbnB1dCgpIHJhZGl1czogbnVtYmVyO1xuICBASW5wdXQoKSBzdHJva2U6IG51bWJlcjtcblxuICBwdWJsaWMgZmFjZVJhZGl1czogbnVtYmVyO1xuICBwdWJsaWMgdGV4dFJhZGl1czogbnVtYmVyO1xuICBwdWJsaWMgY2xvY2tMaW5lczogSUNsb2NrTGluZXNbXTtcbiAgcHVibGljIGNsb2NrVGV4dHM6IGFueVtdO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY2xvY2tMaW5lcyA9IFtdO1xuICAgIHRoaXMuY2xvY2tUZXh0cyA9IFtdO1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoKSB7XG4gICAgdGhpcy5mYWNlUmFkaXVzID0gdGhpcy5yYWRpdXMgLSA1O1xuICAgIHRoaXMudGV4dFJhZGl1cyA9IHRoaXMucmFkaXVzIC0gMjY7XG5cbiAgICB0aGlzLmNyZWF0ZUNsb2NrTGluZXMoKTtcbiAgICB0aGlzLmNyZWF0ZUNsb2NrVGV4dHMoKTtcbiAgfVxuXG4gIHByaXZhdGUgY3JlYXRlQ2xvY2tMaW5lcygpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IERFRkFVTFRfUkFOR0U7IGkrKykge1xuICAgICAgY29uc3QgY29zID0gTWF0aC5jb3MoKCgyICogTWF0aC5QSSkgLyBERUZBVUxUX1JBTkdFKSAqIGkpO1xuICAgICAgY29uc3Qgc2luID0gTWF0aC5zaW4oKCgyICogTWF0aC5QSSkgLyBERUZBVUxUX1JBTkdFKSAqIGkpO1xuICAgICAgdGhpcy5jbG9ja0xpbmVzLnB1c2goe1xuICAgICAgICBpZDogaSxcbiAgICAgICAgc3Ryb2tlV2lkdGg6IGkgJSA0ID09PSAwID8gMyA6IDEsXG4gICAgICAgIHgxOiBjb3MgKiB0aGlzLmZhY2VSYWRpdXMsXG4gICAgICAgIHkxOiBzaW4gKiB0aGlzLmZhY2VSYWRpdXMsXG4gICAgICAgIHgyOiBjb3MgKiAodGhpcy5mYWNlUmFkaXVzIC0gNyksXG4gICAgICAgIHkyOiBzaW4gKiAodGhpcy5mYWNlUmFkaXVzIC0gNylcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgY3JlYXRlQ2xvY2tUZXh0cygpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IERFRkFVTFRfVElNRV9SQU5HRTsgaSsrKSB7XG4gICAgICB0aGlzLmNsb2NrVGV4dHMucHVzaCh7XG4gICAgICAgIGlkOiBpLFxuICAgICAgICB4OlxuICAgICAgICAgIHRoaXMudGV4dFJhZGl1cyAqXG4gICAgICAgICAgTWF0aC5jb3MoKCgyICogTWF0aC5QSSkgLyAxMikgKiBpIC0gTWF0aC5QSSAvIDIgKyBNYXRoLlBJIC8gNiksXG4gICAgICAgIHk6XG4gICAgICAgICAgdGhpcy50ZXh0UmFkaXVzICpcbiAgICAgICAgICBNYXRoLnNpbigoKDIgKiBNYXRoLlBJKSAvIDEyKSAqIGkgLSBNYXRoLlBJIC8gMiArIE1hdGguUEkgLyA2KVxuICAgICAgfSk7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XG5pbXBvcnQgeyBOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudCB9IGZyb20gXCIuL25neC1jcy1zbGlkZXIvbmd4LWNzLXNsaWRlci5jb21wb25lbnRcIjtcbmltcG9ydCB7IE5nWENTQ2xvY2tGYWNlQ29tcG9uZW50IH0gZnJvbSBcIi4vbmd4LWNzLWNsb2NrLWZhY2Uvbmd4LWNzLWNsb2NrLWZhY2UuY29tcG9uZW50XCI7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxuICBkZWNsYXJhdGlvbnM6IFtOZ3hDaXJjdWxhclNsaWRlckNvbXBvbmVudCwgTmdYQ1NDbG9ja0ZhY2VDb21wb25lbnRdLFxuICBleHBvcnRzOiBbTmd4Q2lyY3VsYXJTbGlkZXJDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIE5neENpcmN1bGFyU2xpZGVyTW9kdWxlIHt9XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLEFBd0JBLHVCQUFNLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztBQUM1Qix1QkFBTSxhQUFhLEdBQVc7SUFDNUIsUUFBUSxFQUFFLENBQUM7SUFDWCxXQUFXLEVBQUUsRUFBRTtJQUNmLE1BQU0sRUFBRSxHQUFHO0lBQ1gsaUJBQWlCLEVBQUUsU0FBUztJQUM1QixlQUFlLEVBQUUsU0FBUztJQUMxQixhQUFhLEVBQUUsU0FBUztJQUN4QixhQUFhLEVBQUUsSUFBSTtJQUNuQixjQUFjLEVBQUUsU0FBUztDQUMxQixDQUFDO0FBT0Y7SUErQkU7c0JBMUIwQyxJQUFJLFlBQVksRUFBVztRQTJCbkUsSUFBSSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7S0FDdEI7Ozs7O0lBbEJPLE9BQU8sdUJBQXVCLENBQUMsR0FBNEI7UUFDakUsdUJBQU0sTUFBTSxHQUNWLEdBQUcsWUFBWSxVQUFVO2NBQ3JCO2dCQUNFLENBQUMsRUFBRSxHQUFHLENBQUMsT0FBTztnQkFDZCxDQUFDLEVBQUUsR0FBRyxDQUFDLE9BQU87YUFDZjtjQUNEO2dCQUNFLENBQUMsRUFBRSxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO2dCQUNyQyxDQUFDLEVBQUUsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTzthQUN0QyxDQUFDO1FBQ1IsT0FBTyxNQUFNLENBQUM7Ozs7O0lBU2hCLFFBQVE7UUFDTixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztLQUN2Qjs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBdUI7UUFDakMsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxXQUFXO2tCQUNsQyxNQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQztrQkFDeEQsYUFBYSxDQUFDO1NBQ25CO1FBQ0QsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0tBQ2pCOzs7O0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztLQUNyQjs7OztJQUVPLFFBQVE7UUFDZCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDZixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7WUFDM0IsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO1NBQzlCLENBQUMsQ0FBQzs7Ozs7SUFHRyxjQUFjO1FBQ3BCLHVCQUFNLFVBQVUsR0FBRyxLQUFLLENBQ3RCLFNBQVMsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLEVBQ2hDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQ2pDLENBQUM7UUFDRix1QkFBTSxRQUFRLEdBQUcsS0FBSyxDQUNwQixTQUFTLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxFQUM5QixTQUFTLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUNoQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7O1FBa0JGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQzNCLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsRUFDcEQsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUNwRDthQUNFLElBQUksQ0FDSCxTQUFTLENBQUMsQ0FBQyxJQUNULFVBQVUsQ0FBQyxJQUFJLENBQ2IsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUNuQixZQUFZLENBQUMsZ0JBQWdCLENBQUMsQ0FDL0IsQ0FDRixDQUNGO2FBQ0EsU0FBUyxDQUFDLENBQUMsR0FBNEI7WUFDdEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN6QixDQUFDLENBQUM7Ozs7O0lBR0MsWUFBWTs7Ozs7UUFLbEIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDOUI7Ozs7OztJQUdLLGNBQWMsQ0FBQyxHQUE0QjtRQUNqRCx1QkFBTSxNQUFNLEdBQUcsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFdkUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLHVCQUFNLGdCQUFnQixHQUNwQixDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZELHFCQUFJLFFBQVEsR0FDVixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7WUFDeEUsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFZCxJQUFJLFFBQVEsR0FBRyxDQUFDLEVBQUU7WUFDaEIsUUFBUSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1NBQ3pCO1FBRUQscUJBQUksY0FBYyxHQUFHLGdCQUFnQixHQUFHLFFBQVEsQ0FBQztRQUNqRCxJQUFJLGNBQWMsR0FBRyxDQUFDLEVBQUU7WUFDdEIsY0FBYyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1NBQy9CO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxjQUFjLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVsRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Ozs7OztJQUdWLGFBQWEsQ0FBQyxHQUE0QjtRQUNoRCx1QkFBTSxNQUFNLEdBQUcsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLHVCQUFNLFFBQVEsR0FDWixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7WUFDeEUsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDZCxxQkFBSSxjQUFjLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWxFLElBQUksY0FBYyxHQUFHLENBQUMsRUFBRTtZQUN0QixjQUFjLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7U0FDL0I7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLGNBQWMsQ0FBQztRQUNsQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Ozs7O0lBR1YsZ0JBQWdCO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUNsQyxDQUFDLEVBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUNqQixJQUFJLENBQUMsVUFBVSxFQUNmLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7UUFDRixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FDakMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQ2pCLElBQUksQ0FBQyxVQUFVLEVBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQzs7Ozs7Ozs7O0lBR0ksaUJBQWlCLENBQ3ZCLEtBQUssRUFDTCxRQUFRLEVBQ1IsaUJBQWlCLEVBQ2pCLGVBQWU7UUFFZix1QkFBTSxXQUFXLEdBQUcsY0FBYyxDQUFDLGlCQUFpQixFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBRXZFLE9BQU87WUFDTCxTQUFTLEVBQUUsV0FBVyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUM7WUFDeEMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksUUFBUSxDQUFDO1NBQzdDLENBQUM7Ozs7Ozs7Ozs7SUFHSSxrQkFBa0IsQ0FDeEIsVUFBVSxFQUNWLFFBQVEsRUFDUixNQUFNLEVBQ04sZUFBZSxHQUFHLENBQUMsRUFDbkIsZ0JBQWdCLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFOztRQUc5Qix1QkFBTSxVQUFVLEdBQUcsZUFBZSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbkQsdUJBQU0sV0FBVyxHQUFHLGdCQUFnQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDckQsdUJBQU0sS0FBSyxHQUFHLFVBQVUsR0FBRyxDQUFDLENBQUM7UUFDN0IsdUJBQU0sU0FBUyxHQUFHLENBQUMsV0FBVyxHQUFHLFFBQVEsS0FBSyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEdBQUcsVUFBVSxDQUFDO1FBQ3RFLHVCQUFNLE9BQU8sR0FBRyxDQUFDLFdBQVcsR0FBRyxRQUFRLElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQztRQUM5RCx1QkFBTSxLQUFLLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDM0MsdUJBQU0sS0FBSyxHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUMsdUJBQU0sT0FBTyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNDLHVCQUFNLE9BQU8sR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDOztRQUc1Qyx1QkFBTSxHQUFHLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQy9DLHVCQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsQ0FBQztRQUVoRCxPQUFPO1lBQ0wsS0FBSztZQUNMLEtBQUs7WUFDTCxHQUFHO1lBQ0gsR0FBRztZQUNILE9BQU87WUFDUCxPQUFPO1NBQ1IsQ0FBQzs7Ozs7SUFHSSxjQUFjO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ25CLEtBQUsscUJBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDNUMsdUJBQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNiLHVCQUFNLE1BQU0sR0FBVyxJQUFJLENBQUMsaUJBQWlCLENBQzNDLEVBQUUsRUFDRixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQzNCLENBQUM7WUFDRix1QkFBTSxJQUFJLEdBQVMsSUFBSSxDQUFDLGtCQUFrQixDQUN4QyxFQUFFLEVBQ0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUNqQixJQUFJLENBQUMsVUFBVSxFQUNmLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7WUFFRixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDakIsRUFBRSxFQUFFLEVBQUU7Z0JBQ04sQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQ3BELElBQUksQ0FBQyxLQUFLLENBQUMsTUFDYixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTTtnQkFDYixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDcEQsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQztnQkFDakMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQzthQUM5QixDQUFDLENBQUM7U0FDSjs7Ozs7SUFHSyxlQUFlOztRQUVyQix1QkFBTSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ3ZELHVCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ25FLHVCQUFNLEVBQUUsR0FBRyxRQUFRLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDekMsdUJBQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUN2Qyx1QkFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxHQUFHLGVBQWUsQ0FBQztRQUMxQyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsR0FBRyxlQUFlLENBQUM7Ozs7O0lBR3JDLGlCQUFpQjtRQUN0QixNQUFNLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDM0MsT0FBTyxXQUFXLEdBQUcsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Ozs7OztJQUcvQixhQUFhLENBQUMsS0FBSztRQUN4QixPQUFPLFdBQVcsS0FBSyxFQUFFLENBQUM7Ozs7OztJQUdyQixjQUFjLENBQUMsS0FBSztRQUN6QixPQUFPLE1BQU0sR0FBQyxNQUFNLENBQUMsUUFBUSxHQUFDLFdBQVcsR0FBQyxLQUFLLEdBQUMsR0FBRyxDQUFDOzs7OztJQUcvQyxZQUFZO1FBQ2pCLE9BQU87SUFDUCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQztJQUNsRCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUM7Ozs7Ozs7SUFHbEQsZ0JBQWdCLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUIsT0FBTyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs7OztZQWpTbkMsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxlQUFlO2dCQUN6QixXQUFXLEVBQUUsZ0NBQWdDO2dCQUM3QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQzthQUM5Qzs7Ozs7b0JBR0UsS0FBSzt5QkFDTCxLQUFLOzBCQUNMLEtBQUs7cUJBQ0wsTUFBTTtxQkFRTixTQUFTLFNBQUMsUUFBUTt1QkFDbEIsU0FBUyxTQUFDLFVBQVU7d0JBQ3BCLFNBQVMsU0FBQyxXQUFXOzs7Ozs7O0FDeER4QixBQUVBLHVCQUFNLGFBQWEsR0FBRyxFQUFFLENBQUM7QUFDekIsdUJBQU0sa0JBQWtCLEdBQUcsRUFBRSxDQUFDOztJQXlCNUI7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztLQUN0Qjs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFFbkMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7S0FDekI7Ozs7SUFFTyxnQkFBZ0I7UUFDdEIsS0FBSyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdEMsdUJBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxJQUFJLGFBQWEsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMxRCx1QkFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLElBQUksYUFBYSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUNuQixFQUFFLEVBQUUsQ0FBQztnQkFDTCxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7Z0JBQ2hDLEVBQUUsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVU7Z0JBQ3pCLEVBQUUsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVU7Z0JBQ3pCLEVBQUUsRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7Z0JBQy9CLEVBQUUsRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7YUFDaEMsQ0FBQyxDQUFDO1NBQ0o7Ozs7O0lBR0ssZ0JBQWdCO1FBQ3RCLEtBQUsscUJBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsa0JBQWtCLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDM0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLEVBQUUsRUFBRSxDQUFDO2dCQUNMLENBQUMsRUFDQyxJQUFJLENBQUMsVUFBVTtvQkFDZixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoRSxDQUFDLEVBQ0MsSUFBSSxDQUFDLFVBQVU7b0JBQ2YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNqRSxDQUFDLENBQUM7U0FDSjs7OztZQXJESixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHFCQUFxQjtnQkFDL0IsV0FBVyxFQUFFLG9DQUFvQztnQkFDakQsU0FBUyxFQUFFLENBQUMsb0NBQW9DLENBQUM7YUFDbEQ7Ozs7O3FCQUVFLEtBQUs7cUJBQ0wsS0FBSzs7Ozs7OztBQ3JCUjs7O1lBS0MsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDdkIsWUFBWSxFQUFFLENBQUMsMEJBQTBCLEVBQUUsdUJBQXVCLENBQUM7Z0JBQ25FLE9BQU8sRUFBRSxDQUFDLDBCQUEwQixDQUFDO2FBQ3RDOzs7Ozs7Ozs7Ozs7Ozs7In0=