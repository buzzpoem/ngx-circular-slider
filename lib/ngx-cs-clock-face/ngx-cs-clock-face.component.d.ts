import { OnChanges } from "@angular/core";
export interface IClockLines {
    id: number;
    strokeWidth: number;
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}
export declare class NgXCSClockFaceComponent implements OnChanges {
    radius: number;
    stroke: number;
    faceRadius: number;
    textRadius: number;
    clockLines: IClockLines[];
    clockTexts: any[];
    constructor();
    ngOnChanges(): void;
    private createClockLines();
    private createClockTexts();
}
